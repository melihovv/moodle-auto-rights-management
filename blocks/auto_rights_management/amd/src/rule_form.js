// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @module     block_auto_rights_management/rule_form
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2018 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
define(['jquery'], function ($) {
    return {
        init: function () {
            var $eventSelect = $('#id_rule_form_event_name');

            this.hideExtraEventContextSelects($eventSelect.val());

            var that = this;
            $eventSelect.change(function () {
                that.hideExtraEventContextSelects($eventSelect.val());
            });
        },

        hideExtraEventContextSelects: function (event) {
            var $quizRow = $('#id_rule_form_event_contexts_quiz').closest('.form-group');
            var $poasAssignmentRow = $('#id_rule_form_event_contexts_poasassignment').closest('.form-group');
            var $supervisedRow = $('#id_rule_form_event_contexts_supervised').closest('.form-group');

            if (event === 'mod_quiz\\event\\attempt_started') {
                $quizRow.show();
                $poasAssignmentRow.hide();
                $supervisedRow.hide();
            } else if (event === 'mod_poasassignment\\event\\task_selected') {
                $quizRow.hide();
                $poasAssignmentRow.show();
                $supervisedRow.hide();
            } else if (event === 'block_supervised\\event\\start_session') {
                $quizRow.hide();
                $poasAssignmentRow.hide();
                $supervisedRow.show();
            }
        }
    };
});
