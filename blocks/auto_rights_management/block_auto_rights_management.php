<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

use block_auto_rights_management\affected_users_service;

defined('MOODLE_INTERNAL') || die();

/**
 * Class block_auto_rights_management.
 *
 * The main idea of auto rights management block is to add ability to teacher to automatically manage students rights.
 * For example, it is useful for the following task. When student starts his exam, he should not be allowed to
 * chat or to use forum. When student finishes his exam, his rights should be restored.
 *
 * @package    block
 * @subpackage auto_rights_management
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2018 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_auto_rights_management extends block_base {
    /**
     * @inheritdoc
     */
    public function init() {
        $this->title = get_string('auto_rights_management', 'block_auto_rights_management');
    }

    /**
     * @inheritdoc
     * @throws \moodle_exception
     */
    public function get_content() {
        if ($this->content !== null) {
            return $this->content;
        }

        $text = has_capability('block/auto_rights_management:viewforteacher', $this->context)
            ? $this->content_for_teacher()
            : $this->content_for_student();

        $this->content = (object)[
            'text' => $text,
        ];

        return $this->content;
    }

    /**
     * @return string
     * @throws coding_exception
     * @throws moodle_exception
     */
    private function content_for_teacher() {
        global $COURSE;
        $rulesurl = new moodle_url('/blocks/auto_rights_management/crud/rules/index.php', [
            'blockid' => $this->instance->id,
            'courseid' => $COURSE->id,
        ]);
        $ruleslink = html_writer::link(
            $rulesurl,
            get_string('list_of_rules', 'block_auto_rights_management')
        );
        $usersurl = new moodle_url('/blocks/auto_rights_management/crud/users/index.php', [
            'blockid' => $this->instance->id,
            'courseid' => $COURSE->id,
        ]);
        $userslink = html_writer::link(
            $usersurl,
            get_string('list_of_affected_users', 'block_auto_rights_management')
        );

        return "{$ruleslink}<br>{$userslink}";
    }

    /**
     * @return string
     * @throws coding_exception
     * @throws moodle_exception
     */
    private function content_for_student() {
        global $USER;
        $roles = affected_users_service::create($this->instance->id)->get_user_roles($USER->id);
        $rolescount = count($roles);

        if ($rolescount === 0) {
            return get_string('you_are_not_affected', 'block_auto_rights_management');
        }

        $rolenames = implode(', ', array_map(function ($item) {
            return $item->rolename;
        }, $roles));

        return get_string(
                $rolescount === 1 ? 'your_current_role' : 'your_current_roles',
                'block_auto_rights_management'
            ) . ": {$rolenames}";
    }

    /**
     * @inheritdoc
     */
    public function applicable_formats() {
        return [
            'course-view' => true,
        ];
    }
}
