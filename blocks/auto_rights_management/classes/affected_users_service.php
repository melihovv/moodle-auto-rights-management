<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace block_auto_rights_management;

defined('MOODLE_INTERNAL') || die();

/**
 * Class affected_users_service.
 *
 * @package    block
 * @subpackage auto_rights_management
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2017 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class affected_users_service {
    /** @var \moodle_database */
    private $db;

    /** @var int */
    private $blockid;

    /**
     * @param int $blockid
     */
    public function __construct($blockid) {
        global $DB;

        $this->db = $DB;
        $this->blockid = $blockid;
    }

    /**
     * @param int $blockid
     * @return affected_users_service
     */
    public static function create($blockid) {
        return new static($blockid);
    }

    /**
     * @return array
     * @throws \dml_exception
     */
    public function all() {
        return $this->db->get_records_sql("
            SELECT bu.*,
                   u.firstname,
                   u.lastname,
                   u.firstnamephonetic,
                   u.lastnamephonetic,
                   u.middlename,
                   u.alternatename,
                   br.name AS rulename
              FROM {user} u
              JOIN {block_arm_aff_users} bu ON bu.userid = u.id
              JOIN {block_arm_rules} br ON br.id = bu.ruleid
              WHERE bu.blockid = {$this->blockid}
        ");
    }

    /**
     * @param int $userid
     * @param int $ruleid
     * @return bool|int
     * @throws \dml_exception
     */
    public function mark_user_as_affected($userid, $ruleid) {
        return $this->db->insert_record('block_arm_aff_users', [
            'userid' => $userid,
            'ruleid' => $ruleid,
            'blockid' => $this->blockid,
        ]);
    }

    /**
     * @param int $userid
     * @param int $ruleid
     * @return bool
     * @throws \dml_exception
     */
    public function unmark_user_as_affected($userid, $ruleid) {
        return $this->db->delete_records('block_arm_aff_users', [
            'userid' => $userid,
            'ruleid' => $ruleid,
            'blockid' => $this->blockid,
        ]);
    }

    /**
     * @param int $userid
     * @param int $ruleid
     * @return bool
     * @throws \dml_exception
     */
    public function is_affected($userid) {
        return $this->db->record_exists('block_arm_aff_users', [
            'userid' => $userid,
            'blockid' => $this->blockid,
        ]);
    }

    /**
     * @param int $userid
     * @param int $ruleid
     * @throws \coding_exception
     * @throws \dml_exception
     */
    public function restore_rights($userid, $ruleid) {
        $rule = rules_service::create($this->blockid)->get_by_id($ruleid);
        $rightsmanager = rights_manager::create($this->blockid);
        $dto = rule_dto::from_rule($rule, $userid);

        $rightsmanager->rollback($dto);
    }

    /**
     * @param int $ruleid
     * @throws \coding_exception
     * @throws \dml_exception
     */
    public function restore_rights_of_all_affected_users($ruleid) {
        $users = $this->db->get_records('block_arm_aff_users', [
            'ruleid' => $ruleid,
            'blockid' => $this->blockid,
        ]);

        foreach ($users as $user) {
            $this->restore_rights($user->userid, $ruleid);
        }
    }

    /**
     * @param int $userid
     * @return array
     * @throws \dml_exception
     */
    public function get_user_roles($userid) {
        return $this->db->get_records_sql("
            SELECT br.rolename
              FROM {block_arm_aff_users} bu
              JOIN {block_arm_rules} br ON br.id = bu.ruleid
              WHERE bu.userid = {$userid}
                AND bu.blockid = {$this->blockid}
        ");
    }

    /**
     * @param $ruleid
     * @return bool
     * @throws \dml_exception
     */
    public function are_there_users_affected_by_rule($ruleid) {
        return $this->db->record_exists('block_arm_aff_users', [
            'ruleid' => $ruleid,
            'blockid' => $this->blockid,
        ]);
    }
}
