<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace block_auto_rights_management;

use context;

defined('MOODLE_INTERNAL') || die();

/**
 * Class rights_manager.
 *
 * @package    block
 * @subpackage auto_rights_management
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2017 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class rights_manager {
    /** @var int */
    private $blockid;

    /** @var affected_users_service */
    private $affectedusersservice;

    /**
     * @param int $blockid
     */
    public function __construct($blockid) {
        $this->blockid = $blockid;
        $this->affectedusersservice = affected_users_service::create($blockid);
    }

    /**
     * @param int $blockid
     * @return rights_manager
     */
    public static function create($blockid) {
        return new static($blockid);
    }

    /**
     * @param rule_dto $dto
     * @throws \coding_exception
     * @throws \dml_exception
     */
    public function change_access(rule_dto $dto) {
        if (! $this->user_has_role_which_should_be_affected($dto)) {
            return;
        }

        $roleid = $this->assign_role_to_user($dto->get_role_name(), $dto->get_user_id(), $dto->get_context());

        // We set CAP_PROHIBIT because CAP_PREVENT is not enough to deny permission in given context.
        // If any user role has CAP_ALLOW permission in specified context or child contexts,
        // it has priority over CAP_PREVENT.
        // We deny permission in the specified context and also in all child contexts.
        // We cannot allow permission in context if user has already assigned CAP_PROHIBIT on it or child contexts.

        foreach ($dto->get_capabilities_to_allow() as $capability) {
            if (! get_capability_info($capability)
                || has_capability($capability, $dto->get_context(), $dto->get_user_id())
            ) {
                continue;
            }

            assign_capability($capability, CAP_ALLOW, $roleid, $dto->get_context());
        }

        foreach ($dto->get_capabilities_to_disallow() as $capability) {
            if (! get_capability_info($capability)
                || ! has_capability($capability, $dto->get_context(), $dto->get_user_id())
            ) {
                continue;
            }

            assign_capability($capability, CAP_PROHIBIT, $roleid, $dto->get_context());
        }

        $this->mark_user_as_affected($dto->get_user_id(), $dto->get_rule_id());
    }

    /**
     * @param rule_dto $dto
     * @throws \coding_exception
     * @throws \dml_exception
     */
    public function rollback(rule_dto $dto) {
        if (! $this->user_has_role_which_should_be_affected($dto)) {
            return;
        }

        $this->unassign_role_from_user($dto->get_role_name(), $dto->get_user_id(), $dto->get_context());
        $this->unmark_user_as_affected($dto->get_user_id(), $dto->get_rule_id());
    }

    /**
     * @param string $rolename
     * @param int $userid
     * @param context $context
     * @return int
     * @throws \coding_exception
     * @throws \dml_exception
     */
    private function assign_role_to_user($rolename, $userid, context $context) {
        global $DB;

        $role = $DB->get_record('role', ['shortname' => $rolename]);

        if ($role) {
            $roleid = $role->id;
        } else {
            $roleid = create_role($rolename, $rolename, '');
        }

        role_assign($roleid, $userid, $context);

        return $roleid;
    }

    /**
     * @param string $rolename
     * @param int $userid
     * @param context $context
     * @throws \coding_exception
     * @throws \dml_exception
     */
    private function unassign_role_from_user($rolename, $userid, context $context) {
        global $DB;

        $role = $DB->get_record('role', ['name' => $rolename]);

        if ($role) {
            role_unassign($role->id, $userid, $context->id);
        }
    }

    /**
     * @param rule_dto $dto
     * @return bool
     */
    private function user_has_role_which_should_be_affected(rule_dto $dto) {
        $userid = $dto->get_user_id();
        $roleids = $dto->get_role_ids_to_affect();
        $context = $dto->get_role_ids_to_affect_in_context();

        if (empty($roleids) || ! $context instanceof context) {
            return true;
        }

        foreach ($roleids as $roleid) {
            if (user_has_role_assignment($userid, $roleid, $context->id)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param int $userid
     * @param int $ruleid
     */
    private function mark_user_as_affected($userid, $ruleid) {
        return $this->affectedusersservice->mark_user_as_affected($userid, $ruleid);
    }

    /**
     * @param int $userid
     * @param int $ruleid
     */
    private function unmark_user_as_affected($userid, $ruleid) {
        return $this->affectedusersservice->unmark_user_as_affected($userid, $ruleid);
    }
}
