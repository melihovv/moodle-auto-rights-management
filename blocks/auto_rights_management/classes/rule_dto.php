<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace block_auto_rights_management;

use block_auto_rights_management\exceptions\not_existing_context;
use context;
use dml_missing_record_exception;
use stdClass;

defined('MOODLE_INTERNAL') || die();

/**
 * Class rule_dto.
 *
 * @package    block
 * @subpackage auto_rights_management
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2017 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class rule_dto {
    /** @var int */
    private $userid;

    /** @var array */
    private $capabilitiestoallow = [];

    /** @var array */
    private $capabilitiestodisallow = [];

    /** @var context */
    private $context;

    /** @var array */
    private $roleidstoaffect = [];

    /** @var context|null */
    private $roleidstoaffectincontext;

    /** @var string */
    private $rolename;

    /** @var int */
    private $ruleid;

    /** @var array */
    private $eventcontexts = [];

    /**
     * @return rule_dto
     */
    public static function create() {
        return new static();
    }

    /**
     * @param stdClass $rule
     * @param int $userid
     * @throws \coding_exception
     */
    public static function from_rule(stdClass $rule, $userid) {
        try {
            $context = context::instance_by_id($rule->context);
            $affectincontext = context::instance_by_id($rule->rolestoaffectcontext);
        } catch (dml_missing_record_exception $e) {
            throw not_existing_context::create($rule->id);
        }

        return rule_dto::create()
            ->set_user_id($userid)
            ->set_capabilities_to_allow(
                array_filter(array_map('trim', explode(',', $rule->capabilitiestoallow)))
            )
            ->set_capabilities_to_disallow(
                array_filter(array_map('trim', explode(',', $rule->capabilitiestodisallow)))
            )
            ->set_context($context)
            ->set_role_ids_to_affect(explode(',', $rule->rolestoaffect))
            ->set_role_ids_to_affect_in_context($affectincontext)
            ->set_role_name($rule->rolename)
            ->set_rule_id($rule->id)
            ->set_event_contexts(
                array_filter(array_map('trim', explode(',', $rule->eventcontexts)))
            );
    }

    /**
     * @return int
     */
    public function get_user_id() {
        return $this->userid;
    }


    /**
     * @param int $userid
     * @return rule_dto
     */
    public function set_user_id($userid) {
        $this->userid = $userid;

        return $this;
    }

    /**
     * @return array
     */
    public function get_capabilities_to_allow() {
        return $this->capabilitiestoallow;
    }

    /**
     * @param string[] $capabilitiestoallow
     * @return rule_dto
     */
    public function set_capabilities_to_allow(array $capabilitiestoallow) {
        $this->capabilitiestoallow = $capabilitiestoallow;

        return $this;
    }

    /**
     * @return array
     */
    public function get_capabilities_to_disallow() {
        return $this->capabilitiestodisallow;
    }

    /**
     * @param string[] $capabilitiestodisallow
     * @return rule_dto
     */
    public function set_capabilities_to_disallow(array $capabilitiestodisallow) {
        $this->capabilitiestodisallow = $capabilitiestodisallow;

        return $this;
    }

    /**
     * @return context
     */
    public function get_context() {
        return $this->context;
    }

    /**
     * @param context $context
     * @return rule_dto
     */
    public function set_context(context $context) {
        $this->context = $context;

        return $this;
    }

    /**
     * @return array
     */
    public function get_role_ids_to_affect() {
        return $this->roleidstoaffect;
    }

    /**
     * @param int[] $roleidstoaffect
     * @return rule_dto
     */
    public function set_role_ids_to_affect(array $roleidstoaffect) {
        $this->roleidstoaffect = $roleidstoaffect;

        return $this;
    }

    /**
     * @return context|null
     */
    public function get_role_ids_to_affect_in_context() {
        return $this->roleidstoaffectincontext;
    }

    /**
     * @param context $roleidstoaffectincontext
     * @return rule_dto
     */
    public function set_role_ids_to_affect_in_context(context $roleidstoaffectincontext) {
        $this->roleidstoaffectincontext = $roleidstoaffectincontext;

        return $this;
    }

    /**
     * @return string
     */
    public function get_role_name() {
        return $this->rolename;
    }

    /**
     * @param string $rolename
     * @return rule_dto
     */
    public function set_role_name($rolename) {
        $this->rolename = $rolename;

        return $this;
    }

    /**
     * @return int
     */
    public function get_rule_id() {
        return $this->ruleid;
    }

    /**
     * @param int $ruleid
     */
    public function set_rule_id($ruleid) {
        $this->ruleid = $ruleid;

        return $this;
    }

    /**
     * @return array
     */
    public function get_event_contexts()
    {
        return $this->eventcontexts;
    }

    /**
     * @param array $eventcontexts
     */
    public function set_event_contexts(array $eventcontexts)
    {
        $this->eventcontexts = $eventcontexts;

        return $this;
    }
}
