<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace block_auto_rights_management;

use block_auto_rights_management\exceptions\rule_with_affected_users_is_updating;
use stdClass;

defined('MOODLE_INTERNAL') || die();

/**
 * Class rules_service.
 *
 * @package    block
 * @subpackage auto_rights_management
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2017 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class rules_service {
    /** @var \moodle_database */
    private $db;

    /** @var int */
    private $blockid;

    /**
     * @param int $blockid
     */
    public function __construct($blockid) {
        global $DB;

        $this->db = $DB;
        $this->blockid = $blockid;
    }

    /**
     * @param int $blockid
     * @return rules_service
     */
    public static function create($blockid) {
        return new static($blockid);
    }

    /**
     * @param array $columns
     * @return array
     * @throws \dml_exception
     */
    public function all(array $columns = ['*']) {
        return $this->db->get_records('block_arm_rules', [
            'blockid' => $this->blockid,
        ], '', implode(', ', $columns));
    }

    /**
     * @param $id
     * @return stdClass
     * @throws \dml_exception
     */
    public function get_by_id($id) {
        return $this->db->get_record('block_arm_rules', ['id' => $id]);
    }

    /**
     * @param array $params
     * @return bool|int
     * @throws \dml_exception
     */
    public function store(array $params) {
        $this->ensure_role_exists($params['rolename']);

        return $this->db->insert_record('block_arm_rules', $params);
    }

    /**
     * @param int $ruleid
     * @param array $params
     * @return bool|int
     * @throws \dml_exception
     */
    public function update($ruleid, array $params) {
        if (affected_users_service::create($this->blockid)->are_there_users_affected_by_rule($ruleid)) {
            throw rule_with_affected_users_is_updating::create($ruleid);
        }

        $this->ensure_role_exists($params['rolename']);

        return $this->db->update_record('block_arm_rules', array_merge($params, ['id' => $ruleid]));
    }

    /**
     * @param string $rolename
     * @throws \coding_exception
     * @throws \dml_exception
     */
    private function ensure_role_exists($rolename) {
        $role = $this->db->get_record('role', ['shortname' => $rolename]);

        if (! $role) {
            create_role($rolename, $rolename, '');
        }
    }

    /**
     * @param int $ruleid
     * @return bool
     * @throws \dml_exception
     * @throws \coding_exception
     */
    public function delete($ruleid) {
        affected_users_service::create($this->blockid)->restore_rights_of_all_affected_users($ruleid);

        return $this->db->delete_records('block_arm_rules', ['id' => $ruleid]);
    }
}
