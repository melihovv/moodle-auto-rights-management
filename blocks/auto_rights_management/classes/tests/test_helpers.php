<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    block
 * @subpackage auto_rights_management
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2018 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_auto_rights_management\tests;

defined('MOODLE_INTERNAL') || die();

trait test_helpers {
    /**
     * @return array
     */
    private function setup_course_with_student() {
        $course = $this->create_course();

        return [$this->create_user_and_enrol_him_to_course($course->id), $course];
    }

    /**
     * @return array
     */
    private function setup_course_with_teacher() {
        $course = $this->create_course();

        return [$this->create_user_and_enrol_him_to_course($course->id, 'teacher'), $course];
    }

    /**
     * @return object|stdClass
     */
    private function create_course() {
        $generator = static::getDataGenerator();

        global $COURSE;
        $COURSE = $generator->create_course();

        return $COURSE;
    }

    /**
     * @param $courseid
     * @param string $rolename
     * @return stdClass
     */
    private function create_user_and_enrol_him_to_course($courseid, $rolename = 'student') {
        $student = static::getDataGenerator()->create_user();
        $this->enrol_user($student->id, $courseid, $rolename);

        return $student;
    }

    /**
     * @param int $studentid
     * @param int $courseid
     * @param string $rolename
     */
    private function enrol_user($studentid, $courseid, $rolename = 'student') {
        static::getDataGenerator()->enrol_user($studentid, $courseid, $rolename);
    }
}
