<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    block
 * @subpackage auto_rights_management
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2018 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use block_auto_rights_management\rules_service;

require_once('../../../../config.php');
global $CFG, $DB, $PAGE, $OUTPUT;

$courseid = required_param('courseid', PARAM_INT);
$blockid = required_param('blockid', PARAM_INT);
$ruleid = required_param('ruleid', PARAM_INT);
$confirmed = optional_param('confirmed', 0, PARAM_INT);

if (!$course = $DB->get_record('course', ['id' => $courseid])) {
    print_error('invalidcourse', 'block_auto_rights_management', $courseid);
}

require_login($course);
require_capability('block/auto_rights_management:managerules', $PAGE->context);

if ($confirmed) {
    rules_service::create($blockid)->delete($ruleid);
    $deleteruleurl = new moodle_url('/blocks/auto_rights_management/crud/rules/index.php', [
        'courseid' => $courseid,
        'blockid' => $blockid,
    ]);
    redirect($deleteruleurl);
} else {
    $PAGE->set_url('/blocks/auto_rights_management/crud/rules/delete.php', [
        'courseid' => $courseid,
        'blockid' => $blockid,
        'ruleid' => $ruleid,
        'confirmed' => 0,
    ]);
    $PAGE->set_pagelayout('standard');
    $PAGE->set_heading(get_string('rules', 'block_auto_rights_management'));

    $settingsnode = $PAGE->settingsnav->add(
        get_string('auto_rights_management', 'block_auto_rights_management')
    );
    $deleteruleurl = new moodle_url('/blocks/auto_rights_management/crud/rules/delete.php', [
        'courseid' => $courseid,
        'blockid' => $blockid,
        'ruleid' => $ruleid,
        'confirmed' => 0,
    ]);
    $blocknode = $settingsnode->add(
        get_string('delete_rule', 'block_auto_rights_management'),
        $deleteruleurl
    );
    $blocknode->make_active();

    echo $OUTPUT->header();
    echo $OUTPUT->confirm(
        get_string('delete_rule_confirm', 'block_auto_rights_management'),
        new moodle_url('/blocks/auto_rights_management/crud/rules/delete.php', [
            'courseid' => $courseid,
            'blockid' => $blockid,
            'ruleid' => $ruleid,
            'confirmed' => 1,
        ]),
        new moodle_url('/blocks/auto_rights_management/crud/rules/index.php', [
            'courseid' => $courseid,
            'blockid' => $blockid,
        ])
    );
    echo $OUTPUT->footer();
}
