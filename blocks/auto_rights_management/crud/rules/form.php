<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    block
 * @subpackage auto_rights_management
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2018 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use block_auto_rights_management\exceptions\rule_with_affected_users_is_updating;
use block_auto_rights_management\rule_dto;
use block_auto_rights_management\rules_service;

require_once('../../../../config.php');
require_once __DIR__ . '/rule_form.php';

global $CFG, $DB, $PAGE, $OUTPUT;

$courseid = required_param('courseid', PARAM_INT);
$blockid = required_param('blockid', PARAM_INT);
$ruleid = optional_param('id', 0, PARAM_INT);

if (!$course = $DB->get_record('course', ['id' => $courseid])) {
    print_error('invalidcourse', 'block_auto_rights_management', $courseid);
}

require_login($course);
require_capability('block/auto_rights_management:managerules', $PAGE->context);

$PAGE->set_url('/blocks/auto_rights_management/crud/rules/form.php', [
    'courseid' => $courseid,
    'blockid' => $blockid,
    'id' => $ruleid,
]);
$PAGE->requires->js_call_amd('block_auto_rights_management/rule_form', 'init');
$PAGE->set_pagelayout('standard');
$PAGE->set_heading(get_string($ruleid ? 'edit_rule' :'create_rule', 'block_auto_rights_management'));

$settingsnode = $PAGE->settingsnav->add(
    get_string('rules', 'block_auto_rights_management')
);

$indexnode = $settingsnode->add(
    get_string('rules', 'block_auto_rights_management'),
    $rulesurl = new moodle_url('/blocks/auto_rights_management/crud/rules/index.php', [
        'courseid' => $courseid,
        'blockid' => $blockid,
    ])
);
$pagenode = $indexnode->add(
    get_string($ruleid ? 'edit_rule' :'create_rule', 'block_auto_rights_management'),
    new moodle_url('/blocks/auto_rights_management/crud/rules/form.php', [
        'courseid' => $courseid,
        'blockid' => $blockid,
        'id' => $ruleid,
    ])
);
$pagenode->make_active();

$mform = new rule_form(null, [
    'courseid' => $courseid,
    'blockid' => $blockid,
    'id' => $ruleid,
]);

if ($mform->is_cancelled()) {
    redirect($rulesurl);
} elseif ($data = $mform->get_data()) {
    $capabilitiestoallow = property_exists($data, 'rule_form_capabilities_to_allow')
        ? $data->rule_form_capabilities_to_allow
        : [];
    $capabilitiestodisallow = property_exists($data, 'rule_form_capabilities_to_disallow')
        ? $data->rule_form_capabilities_to_disallow
        : [];
    $modulename = $mform::get_module_name_by_event_name($data->rule_form_event_name);
    $eventcontexts = property_exists($data, "rule_form_event_contexts_$modulename")
        ? $data->{"rule_form_event_contexts_$modulename"}
        : [];
    $params = [
        'blockid' => $data->blockid,
        'name' => $data->rule_form_rule_name,
        'capabilitiestoallow' => implode(',', $capabilitiestoallow),
        'capabilitiestodisallow' => implode(',', $capabilitiestodisallow),
        'context' => $data->rule_form_context,
        'eventname' => $data->rule_form_event_name,
        'rolestoaffect' => implode(',', $data->rule_form_roles_to_affect),
        'rolestoaffectcontext' => $data->rule_form_roles_to_affect_in_context,
        'rolename' => $data->rule_form_role_name,
        'eventcontexts' => implode(',', $eventcontexts),
    ];

    if ($id = $data->id) {
        try {
            rules_service::create($blockid)->update($id, $params);
        } catch (rule_with_affected_users_is_updating $e) {
            show_form($ruleid, $mform, function () {
                $message = get_string(
                    'you_cannot_update_rule_with_affected_users',
                    'block_auto_rights_management'
                );
                echo <<<HERE
<div class="alert alert-danger alert-block fade in " role="alert">
    <button type="button" class="close" data-dismiss="alert">×</button>
    $message
</div>
HERE;
            });
            return;
        }
    } else {
        rules_service::create($blockid)->store($params);
    }

    redirect($rulesurl);
} else {
    if (! $ruleid) {
        show_form($ruleid, $mform);
    }

    $warnings = [];

    $rule = rules_service::create($blockid)->get_by_id($ruleid);
    global $USER;

    try {
        $ruledto = rule_dto::from_rule($rule, $USER->id);

        $filtercb = function ($capability) {
            if (! get_capability_info($capability)) {
                return $capability;
            }
        };

        $notexistingcapabilities = array_merge(
            array_filter($ruledto->get_capabilities_to_allow(), $filtercb),
            array_filter($ruledto->get_capabilities_to_disallow(), $filtercb)
        );

        if ($notexistingcapabilities) {
            $warnings[] = get_string(
                    'you_have_not_existing_capabilities_saved_to_db',
                    'block_auto_rights_management'
                ) . ': ' . implode(', ', $notexistingcapabilities);
        }

        foreach ($ruledto->get_role_ids_to_affect() as $roleid) {
            $role = $DB->get_record('role', ['id' => $roleid]);

            if (! $role) {
                $warnings[] = get_string(
                    'you_have_not_existing_roles_to_affect_saved_to_db',
                    'block_auto_rights_management'
                );
                break;
            }
        }
    } catch (\block_auto_rights_management\exceptions\not_existing_context $e) {
        $warnings[] = get_string(
            'you_have_not_existing_context_saved_to_db',
            'block_auto_rights_management'
        );
    }

    $cb = function () {};

    if ($warnings) {
        $cb = function () use ($warnings) {
            foreach ($warnings as $warning) {
                echo <<<HERE
<div class="alert alert-danger alert-block fade in " role="alert">
    <button type="button" class="close" data-dismiss="alert">×</button>
    $warning
</div>
HERE;
            }
        };
    }

    show_form($ruleid, $mform, $cb);
}

function show_form($ruleid, $mform, $beforecb = null)
{
    global $OUTPUT, $DB;

    if ($ruleid) {
        if (!$rule = $DB->get_record('block_arm_rules', ['id' => $ruleid])) {
            throw new \Exception(null, 404);
        }

        $modulename = $mform::get_module_name_by_event_name($rule->eventname);

        $mform->set_data([
            'rule_form_rule_name' => $rule->name,
            'rule_form_capabilities_to_allow' => array_filter(
                array_map('trim', explode(',', $rule->capabilitiestoallow))
            ),
            'rule_form_capabilities_to_disallow' => array_filter(
                array_map('trim', explode(',', $rule->capabilitiestodisallow))
            ),
            'rule_form_context' => $rule->context,
            'rule_form_event_name' => $rule->eventname,
            'rule_form_roles_to_affect' => explode(',', $rule->rolestoaffect),
            'rule_form_roles_to_affect_in_context' => $rule->rolestoaffectcontext,
            'rule_form_role_name' => $rule->rolename,
            "rule_form_event_contexts_$modulename" => array_filter(
                array_map('trim', explode(',', $rule->eventcontexts))
            ),
        ]);
    }

    echo $OUTPUT->header();

    if ($beforecb) {
        $beforecb();
    }

    $mform->display();
    echo $OUTPUT->footer();
}
