<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    block
 * @subpackage auto_rights_management
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2018 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');

/**
 * Class block_auto_rights_management_edit_form
 *
 * Class for edit block settings.
 *
 * @package    block
 * @subpackage auto_rights_management
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2018 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class rule_form extends moodleform {
    const EVENT_QUIZ_ATTEMPT_STARTED = 'mod_quiz\event\attempt_started';
    const EVENT_QUIZ_ATTEMPT_SUBMITTED = 'mod_quiz\event\attempt_submitted';
    const EVENT_QUIZ_ATTEMPT_BECAMEOVERDUE = 'mod_quiz\event\attempt_becameoverdue';
    const EVENT_POAS_TASK_SELECTED = 'mod_poasassignment\event\task_selected';
    const EVENT_POAS_ASSESSABLE_SUBMITTED = 'mod_poasassignment\event\assessable_submitted';
    const EVENT_SUPERVISED_START_SESSION = 'block_supervised\event\start_session';
    const EVENT_SUPERVISED_START_PLANNED_SESSION = 'block_supervised\event\start_planned_session';
    const EVENT_SUPERVISED_DELETE_SESSION = 'block_supervised\event\delete_session';
    const EVENT_SUPERVISED_FINISH_SESSION = 'block_supervised\event\finish_session';

    const MODULE_QUIZ = 'quiz';
    const MODULE_POASASSIGNMENT = 'poasassignment';
    const MODULE_SUPERVISED = 'supervised';

    const MODULES = [
        self::MODULE_QUIZ,
        self::MODULE_POASASSIGNMENT,
        self::MODULE_SUPERVISED,
    ];

    /** @var int */
    private $courseid;

    /** @var int */
    private $id;

    /**
     * @throws \HTML_QuickForm_Error
     * @throws coding_exception
     * @throws dml_exception
     * @throws PEAR_Error
     */
    protected function definition() {
        $mform = $this->_form;

        $this->courseid = $this->_customdata['courseid'];
        $this->id = $this->_customdata['id'];

        $this->hidden_inputs_definition($mform);
        $this->rule_name_definition($mform);
        $this->capabilites_to_allow_definition($mform);
        $this->capabilites_to_disallow_definition($mform);
        $this->context_definition($mform);
        $this->event_name_definition($mform);
        $this->event_contexts_definition($mform);
        $this->roles_to_affect_definition($mform);
        $this->roles_to_affect_in_context_definition($mform);
        $this->role_name_definition($mform);
        $this->buttons_definition($mform);
    }

    /**
     * @param MoodleQuickForm $mform
     * @throws coding_exception
     */
    private function hidden_inputs_definition($mform) {
        $mform->addElement('hidden', 'courseid', $this->_customdata['courseid']);
        $mform->setType('courseid', PARAM_INT);

        $mform->addElement('hidden', 'blockid', $this->_customdata['blockid']);
        $mform->setType('blockid', PARAM_INT);

        $mform->addElement('hidden', 'id', $this->_customdata['id']);
        $mform->setType('id', PARAM_INT);
    }

    /**
     * @param MoodleQuickForm $mform
     * @throws \HTML_QuickForm_Error
     * @throws coding_exception
     * @throws PEAR_Error
     */
    private function rule_name_definition($mform) {
        $mform->addElement('text', 'rule_form_rule_name', get_string('rule_form_rule_name', 'block_auto_rights_management'));
        $mform->setType('rule_form_rule_name', PARAM_ALPHANUMEXT);
        $mform->addHelpButton('rule_form_rule_name', 'rule_form_rule_name', 'block_auto_rights_management');
    }

    /**
     * @param $data
     * @return array
     * @throws coding_exception
     */
    private function rule_name_validation($data) {
        $errors = [];

        if (! isset($data['rule_form_rule_name']) || empty($data['rule_form_rule_name'])) {
            $errors['rule_form_rule_name'] = get_string('rule_form_rule_name_required', 'block_auto_rights_management');
        } else {
            global $DB;
            $rule = $DB->get_record('block_arm_rules', [
                'name' => $data['rule_form_rule_name'],
                'blockid' => $this->_customdata['blockid'],
            ]);

            if ($rule && $rule->id != $this->id) {
                $errors['rule_form_rule_name'] = get_string('rule_form_rule_name_not_unique', 'block_auto_rights_management');
            }
        }

        return $errors;
    }

    /**
     * @param MoodleQuickForm $mform
     * @throws coding_exception
     * @throws HTML_QuickForm_Error
     */
    private function capabilites_to_allow_definition($mform) {
        $caps = [];
        foreach (get_all_capabilities() as $cap) {
            $caps[$cap['name']] = "{$cap['name']}: " . get_capability_string($cap['name']);
        }

        $mform->addElement(
            'autocomplete',
            'rule_form_capabilities_to_allow',
            get_string('rule_form_capabilities_to_allow', 'block_auto_rights_management'),
            $caps,
            [
                'multiple' => true,
            ]
        );
        $mform->addHelpButton(
            'rule_form_capabilities_to_allow',
            'rule_form_capabilities_to_allow',
            'block_auto_rights_management'
        );
    }

    /**
     * @param $data
     * @return array
     * @throws coding_exception
     */
    private function capabilities_to_allow_validation($data) {
        $errors = [];

        if (
            (! isset($data['rule_form_capabilities_to_allow']) || ! is_array($data['rule_form_capabilities_to_allow']))
            &&
            (! isset($data['rule_form_capabilities_to_disallow']) || ! is_array($data['rule_form_capabilities_to_disallow']))
        ) {
            $errors['rule_form_capabilities_to_allow'] = get_string(
                'rule_form_capabilities_required',
                'block_auto_rights_management'
            );
        } else if (isset($data['rule_form_capabilities_to_allow'])
            && is_array($data['rule_form_capabilities_to_allow'])
        ) {
            if (isset($data['rule_form_capabilities_to_disallow'])
                && is_array($data['rule_form_capabilities_to_disallow'])
            ) {
                $intersection = array_intersect(
                    $data['rule_form_capabilities_to_allow'],
                    $data['rule_form_capabilities_to_disallow']
                );

                if ($intersection) {
                    $errors['rule_form_capabilities_to_allow'] = get_string(
                        'rule_form_capabilities_cannot_allow_and_disallow_the_same_capabilities',
                        'block_auto_rights_management'
                    );

                    return $errors;
                }
            }

            $capnames = array_map(function ($cap) {
                return $cap['name'];
            }, get_all_capabilities());

            foreach ($data['rule_form_capabilities_to_allow'] as $cap) {
                if (! in_array($cap, $capnames, false)) {
                    $errors['rule_form_capabilities_to_allow'] = get_string(
                        'rule_form_capabilities_not_exist',
                        'block_auto_rights_management'
                    );
                }
            }
        }

        return $errors;
    }

    /**
     * @param MoodleQuickForm $mform
     * @throws coding_exception
     * @throws HTML_QuickForm_Error
     */
    private function capabilites_to_disallow_definition($mform) {
        $caps = [];
        foreach (get_all_capabilities() as $cap) {
            $caps[$cap['name']] = "{$cap['name']}: " . get_capability_string($cap['name']);
        }

        $mform->addElement(
            'autocomplete',
            'rule_form_capabilities_to_disallow',
            get_string('rule_form_capabilities_to_disallow', 'block_auto_rights_management'),
            $caps,
            [
                'multiple' => true,
            ]
        );
        $mform->addHelpButton(
            'rule_form_capabilities_to_disallow',
            'rule_form_capabilities_to_disallow',
            'block_auto_rights_management'
        );
    }

    /**
     * @param $data
     * @return array
     * @throws coding_exception
     */
    private function capabilities_to_disallow_validation($data) {
        $errors = [];

        if (isset($data['rule_form_capabilities_to_disallow'])
            && is_array($data['rule_form_capabilities_to_disallow'])
        ) {
            $capnames = array_map(function ($cap) {
                return $cap['name'];
            }, get_all_capabilities());

            foreach ($data['rule_form_capabilities_to_disallow'] as $cap) {
                if (! in_array($cap, $capnames, false)) {
                    $errors['rule_form_capabilities_to_disallow'] = get_string(
                        'rule_form_capabilities_not_exist',
                        'block_auto_rights_management'
                    );
                }
            }
        }

        return $errors;
    }

    /**
     * @param MoodleQuickForm $mform
     * @throws HTML_QuickForm_Error
     * @throws coding_exception
     * @throws dml_exception
     */
    private function context_definition($mform) {
        $mform->addElement(
            'select',
            'rule_form_context',
            get_string('rule_form_context', 'block_auto_rights_management'),
            $this->get_context_tree_for_select(true)
        );
        $mform->setType('rule_form_context', PARAM_INT);
        $mform->addHelpButton('rule_form_context', 'rule_form_context', 'block_auto_rights_management');
    }

    /**
     * @param $data
     * @return array
     * @throws coding_exception
     * @throws dml_exception
     */
    private function context_validation($data) {
        $errors = [];

        if (! isset($data['rule_form_context']) || empty($data['rule_form_context'])) {
            $errors['rule_form_context'] = get_string('rule_form_context_required', 'block_auto_rights_management');
        } else {
            if (! in_array($data['rule_form_context'], $this->get_context_tree_ids(true))) {
                $errors['rule_form_context'] = get_string('rule_form_context_not_exist', 'block_auto_rights_management');
            }
        }

        return $errors;
    }

    /**
     * @param MoodleQuickForm $mform
     * @throws HTML_QuickForm_Error
     * @throws coding_exception
     * @throws dml_exception
     */
    private function event_name_definition($mform) {
        $mform->addElement(
            'select',
            'rule_form_event_name',
            get_string('rule_form_event_name', 'block_auto_rights_management'),
            $this->get_available_events()
        );
        $mform->addHelpButton('rule_form_event_name', 'rule_form_event_name', 'block_auto_rights_management');
    }

    /**
     * @param $data
     * @return array
     * @throws coding_exception
     * @throws dml_exception
     */
    private function event_name_validation($data) {
        $errors = [];

        if (! isset($data['rule_form_event_name']) || empty($data['rule_form_event_name'])) {
            $errors['rule_form_event_name'] = get_string('rule_form_event_name_required', 'block_auto_rights_management');
        } else {
            if (! in_array($data['rule_form_event_name'], array_keys($this->get_available_events()), false)) {
                $errors['rule_form_event_name'] = get_string('rule_form_event_name_not_exist', 'block_auto_rights_management');
            }
        }

        return $errors;
    }

    /**
     * @param MoodleQuickForm $mform
     * @throws HTML_QuickForm_Error
     * @throws coding_exception
     * @throws dml_exception
     */
    private function event_contexts_definition($mform) {
        foreach (self::MODULES as $modulename) {
            $select = $mform->addElement(
                'select',
                "rule_form_event_contexts_$modulename",
                get_string('rule_form_event_contexts', 'block_auto_rights_management'),
                $this->get_module_event_contexts($modulename)
            );
            $select->setMultiple(true);
            $mform->addHelpButton(
                "rule_form_event_contexts_$modulename",
                'rule_form_event_contexts',
                'block_auto_rights_management'
            );
        }
    }

    /**
     * @param $data
     * @return array
     * @throws coding_exception
     * @throws dml_exception
     */
    private function event_contexts_validation($data) {
        if (! isset($data['rule_form_event_name'])) {
            return [];
        }

        $modulename = self::get_module_name_by_event_name($data['rule_form_event_name']);

        if (! $modulename) {
            return [];
        }

        $moduleids = isset($data["rule_form_event_contexts_$modulename"])
            ? $data["rule_form_event_contexts_$modulename"]
            : [];

        if (empty($moduleids)) {
            return [];
        }

        $moduleidsofcurrentevent = array_keys($this->get_module_event_contexts($modulename));

        foreach ($moduleids as $moduleid) {
            if (! in_array($moduleid, $moduleidsofcurrentevent)) {
                $errors["rule_form_event_contexts_$modulename"] = get_string(
                    'rule_form_event_contexts_not_exists',
                    'block_auto_rights_management'
                );
            }
        }

        return [];
    }


    /**
     * @param MoodleQuickForm $mform
     * @throws \HTML_QuickForm_Error
     * @throws coding_exception
     * @throws PEAR_Error
     */
    private function roles_to_affect_definition($mform) {
        global $DB;
        $roles = $DB->get_records('role', null, 'sortorder', 'id,shortname');

        $rolesforselect = [];
        foreach ($roles as $role) {
            $rolesforselect[$role->id] = $role->shortname;
        }

        $mform
            ->addElement('select', 'rule_form_roles_to_affect', get_string('rule_form_roles_to_affect', 'block_auto_rights_management'), $rolesforselect)
            ->setMultiple(true);
        $mform->addHelpButton('rule_form_roles_to_affect', 'rule_form_roles_to_affect', 'block_auto_rights_management');
    }

    /**
     * @param $data
     * @return array
     * @throws coding_exception
     */
    private function roles_to_affect_validation($data) {
        $errors = [];

        if (! isset($data['rule_form_roles_to_affect']) || ! is_array($data['rule_form_roles_to_affect'])) {
            $errors['rule_form_roles_to_affect'] = get_string('rule_form_roles_to_affect_required', 'block_auto_rights_management');
            return $errors;
        }

        global $DB;

        if (! $data) {
            $errors['rule_form_roles_to_affect'] = get_string(
                'rule_form_roles_to_affect_required',
                'block_auto_rights_management'
            );
            return $errors;
        }

        foreach ($data['rule_form_roles_to_affect'] as $role) {
            $roleexists = $DB->record_exists('role', ['id' => $role]);

            if (!$roleexists) {
                $errors['rule_form_roles_to_affect'] = get_string(
                    'rule_form_roles_to_affect_not_exists',
                    'block_auto_rights_management'
                );
                break;
            }
        }

        return $errors;
    }

    /**
     * @param MoodleQuickForm $mform
     * @throws \HTML_QuickForm_Error
     * @throws coding_exception
     * @throws PEAR_Error
     */
    private function roles_to_affect_in_context_definition($mform) {
        $mform->addElement(
            'select',
            'rule_form_roles_to_affect_in_context',
            get_string('rule_form_roles_to_affect_in_context', 'block_auto_rights_management'),
            $this->get_context_tree_for_select(false)
        );
        $mform->setType('rule_form_roles_to_affect_in_context', PARAM_INT);
        $mform->addHelpButton('rule_form_roles_to_affect_in_context', 'rule_form_roles_to_affect_in_context', 'block_auto_rights_management');
    }

    /**
     * @param $data
     * @return array
     * @throws coding_exception
     */
    private function roles_to_affect_in_context_validation($data) {
        $errors = [];

        if (! isset($data['rule_form_roles_to_affect_in_context'])
            || empty($data['rule_form_roles_to_affect_in_context'])
        ) {
            $errors['rule_form_roles_to_affect_in_context'] = get_string(
                'rule_form_roles_to_affect_in_context_required',
                'block_auto_rights_management'
            );
            return $errors;
        }

        if (! in_array($data['rule_form_roles_to_affect_in_context'], $this->get_context_tree_ids(false))) {
            $errors['rule_form_roles_to_affect_in_context'] = get_string(
                'rule_form_roles_to_affect_in_context_not_exist',
                'block_auto_rights_management'
            );
        }

        return $errors;
    }

    /**
     * @param MoodleQuickForm $mform
     * @throws \HTML_QuickForm_Error
     * @throws coding_exception
     * @throws PEAR_Error
     */
    private function role_name_definition($mform) {
        $mform->addElement('text', 'rule_form_role_name', get_string('rule_form_role_name', 'block_auto_rights_management'));
        $mform->setType('rule_form_role_name', PARAM_ALPHANUMEXT);
        $mform->addHelpButton('rule_form_role_name', 'rule_form_role_name', 'block_auto_rights_management');
    }

    /**
     * @param $data
     * @return array
     * @throws coding_exception
     */
    private function role_name_validation($data) {
        $errors = [];

        if (! isset($data['rule_form_role_name']) || empty($data['rule_form_role_name'])) {
            $errors['rule_form_role_name'] = get_string('rule_form_role_name_required', 'block_auto_rights_management');
        } else {
            global $DB;

            $roleexists = $DB->record_exists('role', ['shortname' => $data['rule_form_role_name']]);
            $rule = $DB->get_record('block_arm_rules', ['rolename' => $data['rule_form_role_name']]);

            if (($roleexists || $rule) && $rule && $rule->id != $this->id) {
                $errors['rule_form_role_name'] = get_string('rule_form_role_name_not_unique', 'block_auto_rights_management');
            }
        }

        return $errors;
    }

    /**
     * @param MoodleQuickForm $mform
     * @throws coding_exception
     */
    private function buttons_definition($mform) {
        $buttonarray = [];
        $buttonarray[] = &$mform->createElement(
            'submit',
            'submitbutton',
            get_string('save', 'block_auto_rights_management')
        );
        $buttonarray[] = &$mform->createElement('cancel');
        $mform->addGroup($buttonarray, 'buttonar', '', [' '], false);
        $mform->closeHeaderBefore('buttonar');
    }

    /**
     * @inheritdoc
     * @throws \coding_exception
     * @throws \dml_exception
     */
    public function validation($data, $files) {
        return array_merge(
            $this->rule_name_validation($data),
            $this->context_validation($data),
            $this->event_name_validation($data),
            $this->event_contexts_validation($data),
            $this->capabilities_to_allow_validation($data),
            $this->capabilities_to_disallow_validation($data),
            $this->roles_to_affect_validation($data),
            $this->roles_to_affect_in_context_validation($data),
            $this->role_name_validation($data)
        );
    }

    /**
     * @param bool $onlyavailable
     * @return array
     * @throws dml_exception
     */
    private function get_context_tree_for_select($onlyavailable) {
        $contextidtonamemap = [];

        foreach ($this->get_contexts_for_user($onlyavailable) as $context) {
            $contextidtonamemap[$context->id] = $context->get_context_name();
        }

        return $contextidtonamemap;
    }

    /**
     * @param bool $onlyavailable
     * @return array
     * @throws dml_exception
     */
    private function get_context_tree_ids($onlyavailable) {
        return array_map(function ($context) {
            return $context->id;
        }, $this->get_contexts_for_user($onlyavailable));
    }

    /**
     * @param bool $onlyavailable
     * @return array
     * @throws dml_exception
     */
    private function get_contexts_for_user($onlyavailable) {
        $course = get_course($this->courseid);
        $coursecontext = context_course::instance($course->id);
        $contexts = [
            context_system::instance(),
            context_coursecat::instance($course->category),
            $coursecontext,
        ];

        foreach ($coursecontext->get_child_contexts() as $context) {
            $contexts[] = $context;
        }

        global $USER;

        return $onlyavailable
            ? array_filter($contexts, function ($context) use ($USER) {
                return has_capability('mod/assign:grade', $context, $USER);
            })
            : $contexts;
    }

    /**
     * @return array|\context[]
     * @throws coding_exception
     */
    private function get_available_events() {
        $events = [];

        $pluginman = \core_plugin_manager::instance();

        if ($pluginman->get_plugin_info(self::MODULE_QUIZ)
            && $this->course_has_module_instance(self::MODULE_QUIZ)
        ) {
            $events[self::EVENT_QUIZ_ATTEMPT_STARTED] = get_string(
                'rule_form_event_name_quiz_started',
                'block_auto_rights_management'
            );
        }

        if ($pluginman->get_plugin_info(self::MODULE_POASASSIGNMENT)
            && $this->course_has_module_instance(self::MODULE_POASASSIGNMENT)
        ) {
            $events[self::EVENT_POAS_TASK_SELECTED] = get_string(
                'rule_form_event_name_poas_task_started',
                'block_auto_rights_management'
            );
        }

        if ($pluginman->get_plugin_info('block_supervised')
            && $this->course_has_block_instance(self::MODULE_SUPERVISED)
        ) {
            $events[self::EVENT_SUPERVISED_START_SESSION] = get_string(
                'rule_form_event_name_supervised_session_started',
                'block_auto_rights_management'
            );
        }

        return $events;
    }

    /**
     * @param string $modulename
     * @throws dml_exception
     */
    private function course_has_module_instance($modulename) {
        global $DB;
        $record = $DB->get_record('modules', ['name' => $modulename]);

        if (! $record) {
            return false;
        }

        return !! $DB->record_exists('course_modules', [
            'course' => $this->courseid,
            'module' => $record->id,
        ]);
    }

    /**
     * @param string $blockname
     * @throws dml_exception
     */
    private function course_has_block_instance($blockname) {
        global $DB;

        return !! $DB->record_exists('block_instances', [
            'blockname' => $blockname,
            'parentcontextid' => context_course::instance($this->courseid)->id,
        ]);
    }

    /**
     * @param string $module
     * @return array
     */
    private function get_module_event_contexts($modulename) {
        global $DB;

        switch ($modulename) {
            case self::MODULE_QUIZ:
            case self::MODULE_POASASSIGNMENT:
                $record = $DB->get_record('modules', ['name' => $modulename]);

                if (! $record) {
                    return [];
                }

                $coursemodules = $DB->get_records('course_modules', [
                    'course' => $this->courseid,
                    'module' => $record->id,
                ]);

                if (count($coursemodules) === 0) {
                    return [];
                }

                $coursemodulesidsasstring = implode(',', array_map(function ($coursemodule) {
                    return $coursemodule->id;
                }, $coursemodules));
                $contextlevel = CONTEXT_MODULE;
                $contexts = $DB->get_records_sql("
                    SELECT *
                      FROM {context}
                     WHERE instanceid IN ($coursemodulesidsasstring)
                       AND contextlevel=$contextlevel
                ");

                $eventcontexts = [];
                $modinfo = get_fast_modinfo($this->courseid);

                foreach ($coursemodules as $coursemodule) {
                    $foundcontexts = array_filter($contexts, function ($context) use ($coursemodule) {
                        return $context->instanceid == $coursemodule->id;
                    });

                    if (count($foundcontexts) === 0) {
                        throw new RuntimeException("Course module [$coursemodule->id] without context.");
                    }

                    $contextid = reset($foundcontexts)->id;
                    $eventcontexts[$contextid] = $modinfo->cms[$coursemodule->id]->name;
                }

                return $eventcontexts;
            case self::MODULE_SUPERVISED:
                $records = $DB->get_records('block_supervised_lessontype', [
                    'courseid' => $this->courseid,
                ]);

                return array_map(function ($record) {
                    return $record->name;
                }, $records);
            default:
                return [];
        }
    }

    public static function get_module_name_by_event_name($eventname) {
        return [
            self::EVENT_QUIZ_ATTEMPT_STARTED => self::MODULE_QUIZ,
            self::EVENT_POAS_TASK_SELECTED => self::MODULE_POASASSIGNMENT,
            self::EVENT_SUPERVISED_START_SESSION => self::MODULE_SUPERVISED,
        ][$eventname];
    }
}
