<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    block
 * @subpackage auto_rights_management
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2018 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use block_auto_rights_management\affected_users_service;

require_once('../../../../config.php');
global $CFG, $DB, $PAGE, $OUTPUT;
require_once($CFG->libdir . '/tablelib.php');

$courseid = required_param('courseid', PARAM_INT);
$blockid = required_param('blockid', PARAM_INT);

if (!$course = $DB->get_record('course', ['id' => $courseid])) {
    print_error('invalidcourse', 'block_auto_rights_management', $courseid);
}

require_login($course);

$PAGE->set_url('/blocks/auto_rights_management/crud/users/index.php', [
    'courseid' => $courseid,
    'blockid' => $blockid,
]);
$PAGE->set_pagelayout('standard');
$PAGE->set_heading(get_string('users', 'block_auto_rights_management'));

require_capability('block/auto_rights_management:manageusers', $PAGE->context);

$settingsnode = $PAGE->settingsnav->add(
    get_string('auto_rights_management', 'block_auto_rights_management')
);
$usersurl = new moodle_url('/blocks/auto_rights_management/crud/users/index.php', [
    'courseid' => $courseid,
    'blockid' => $blockid,
]);
$blocknode = $settingsnode->add(
    get_string('users', 'block_auto_rights_management'),
    $usersurl
);
$blocknode->make_active();

echo $OUTPUT->header();

$instance = $DB->get_record('block_instances', ['id' => $blockid]);
$block = block_instance('auto_rights_management', $instance);

$table = new flexible_table('block_auto_rights_management');
$table->baseurl = $PAGE->url;
$table->define_columns(['user', 'rule', 'actions']);
$table->define_headers([
    get_string('fullname', 'moodle'),
    get_string('rule', 'block_auto_rights_management'),
    get_string('actions', 'block_auto_rights_management'),
]);
$table->collapsible(true);
$table->initialbars(false);
$table->set_attribute('width', '100%');
$table->setup();

$data = affected_users_service::create($blockid)->all();

foreach ($data as $row) {
    $rulelink = html_writer::link(
        new moodle_url('/blocks/auto_rights_management/crud/rules/form.php', [
            'courseid' => $courseid,
            'blockid' => $blockid,
            'id' => $row->ruleid,
        ]),
        $row->rulename
    );
    $restorerightslink = html_writer::link(
        new moodle_url('/blocks/auto_rights_management/crud/users/restore.php', [
            'courseid' => $courseid,
            'blockid' => $blockid,
            'ruleid' => $row->ruleid,
            'userid' => $row->userid,
        ]),
        get_string('restore_rights', 'block_auto_rights_management'),
        [
            'class' => 'btn btn-default',
        ]
    );

    $table->add_data_keyed([
        'user' => fullname($row),
        'rule' => $rulelink,
        'actions' => $restorerightslink,
    ]);
}

$table->finish_output();

echo $OUTPUT->footer();
