<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    block
 * @subpackage auto_rights_management
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2018 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use block_auto_rights_management\affected_users_service;

require_once('../../../../config.php');
global $DB, $PAGE;

$courseid = required_param('courseid', PARAM_INT);
$blockid = required_param('blockid', PARAM_INT);
$userid = required_param('userid', PARAM_INT);
$ruleid = required_param('ruleid', PARAM_INT);

if (! $course = $DB->get_record('course', ['id' => $courseid])) {
    print_error('invalidcourse', 'block_auto_rights_management', $courseid);
}

if (! $DB->get_record('block_arm_rules', ['id' => $ruleid])) {
    print_error('invalidrule', 'block_auto_rights_management', $ruleid);
}

if (! $DB->get_record('user', ['id' => $userid])) {
    print_error('invaliduser', 'block_auto_rights_management', $userid);
}

require_login($course);

$PAGE->set_url('/blocks/auto_rights_management/crud/users/restore.php', [
    'courseid' => $courseid,
    'blockid' => $blockid,
    'ruleid' => $ruleid,
    'userid' => $userid,
]);

require_capability('block/auto_rights_management:manageusers', $PAGE->context);

$instance = $DB->get_record('block_instances', ['id' => $blockid]);
$block = block_instance('auto_rights_management', $instance);

affected_users_service::create($blockid)->restore_rights($userid, $ruleid);

$affectedusersurl = new moodle_url('/blocks/auto_rights_management/crud/users/index.php', [
    'courseid' => $courseid,
    'blockid' => $blockid,
]);
redirect($affectedusersurl);
