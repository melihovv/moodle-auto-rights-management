<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    block
 * @subpackage auto_rights_management
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2018 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Auto rights management block';
$string['auto_rights_management'] = 'Auto rights management';

$string['auto_rights_management:addinstance'] = 'Add a new auto rights management block';
$string['auto_rights_management:viewforteacher'] = 'View auto rights management block';
$string['auto_rights_management:managerules'] = 'Manage rules';
$string['auto_rights_management:manageusers'] = 'Manage users';

$string['rule_form_rule_name'] = 'Rule name';
$string['rule_form_rule_name_help'] = 'For example, "Restrict communication during exam".';
$string['rule_form_rule_name_required'] = 'Specify rule name';
$string['rule_form_rule_name_not_unique'] = 'Rule name is already taken';

$string['rule_form_context'] = 'Context';
$string['rule_form_context_help'] = 'In which context this rule should work.';
$string['rule_form_context_required'] = 'Select context';
$string['rule_form_context_not_exist'] = 'Selected context does not exist';

$string['rule_form_event_name'] = 'Event';
$string['rule_form_event_name_help'] = 'After which event this rule should do its work.';
$string['rule_form_event_name_required'] = 'Select event';
$string['rule_form_event_name_not_exist'] = 'Selected event does not exist';
$string['rule_form_event_name_quiz_started'] = 'Quiz is started';
$string['rule_form_event_name_poas_task_started'] = 'Poasassignment task is started';
$string['rule_form_event_name_supervised_session_started'] = 'Supervised session is started';

$string['rule_form_event_contexts'] = 'Event context';
$string['rule_form_event_contexts_help'] = 'Context in that event should be fired.';
$string['rule_form_event_contexts_not_exists'] = 'Selected context does not exist for specified event.';
$string['you_have_not_existing_context_saved_to_db'] = 'Somehow you have not existing context saved into database (maybe due to Moodle update)';

$string['rule_form_capabilities_to_allow'] = 'Capabilities to allow';
$string['rule_form_capabilities_to_allow_help'] = 'Which rights should be allowed.';
$string['rule_form_capabilities_required'] = 'Select capabilities, which should be allowed or disallowed';
$string['rule_form_capabilities_cannot_allow_and_disallow_the_same_capabilities'] = 'Cannot simultaneously allow and disallow the same rights.';
$string['rule_form_capabilities_not_exist'] = 'Selected capabilities do not exist';

$string['you_have_not_existing_capabilities_saved_to_db'] = 'Somehow you have not existing capabilities saved into database (maybe due to Moodle update)';

$string['rule_form_capabilities_to_disallow'] = 'Capabilities to disallow';
$string['rule_form_capabilities_to_disallow_help'] = 'Which rights should be disallowed.';

$string['rule_form_roles_to_affect'] = 'Affect users with role';
$string['rule_form_roles_to_affect_help'] = 'This rule will be applied only on users with selected roles.';
$string['rule_form_roles_to_affect_required'] = 'Select roles to affect';
$string['rule_form_roles_to_affect_not_exists'] = 'One of roles does not exist';
$string['you_have_not_existing_roles_to_affect_saved_to_db'] = 'Somehow you have not existing user roles saved into database (maybe due to Moodle update)';

$string['rule_form_roles_to_affect_in_context'] = 'Affect users with role in context';
$string['rule_form_roles_to_affect_in_context_help'] = 'This rule will be applied only on users with selected above roles in specified context.';
$string['rule_form_roles_to_affect_in_context_required'] = 'Select context of roles to affect';
$string['rule_form_roles_to_affect_in_context_not_exist'] = 'Selected context does not exist';

$string['rule_form_role_name'] = 'Role name, which will be assigned to user';
$string['rule_form_role_name_help'] = 'For example, "Student during exam".';
$string['rule_form_role_name_required'] = 'Specify role name';
$string['rule_form_role_name_not_unique'] = 'Role name is already taken';

$string['list_of_rules'] = 'Rules';
$string['list_of_affected_users'] = 'Affected users';

$string['rules'] = 'Rules';
$string['delete_rule'] = 'Delete rule';
$string['delete_rule_confirm'] = 'Are you sure want to delete rule? All rights of affected users will be restored.';
$string['rule'] = 'Rule';
$string['actions'] = 'Actions';
$string['edit'] = 'Edit';
$string['delete'] = 'Delete';
$string['create'] = 'Create';
$string['create_rule'] = 'Create rule';
$string['edit_rule'] = 'Edit rule';
$string['save'] = 'Save';
$string['users'] = 'Users';
$string['user'] = 'User';
$string['restore_rights'] = 'Restore previous rights';

$string['you_are_not_affected'] = 'You does not have any extra role at the moment';
$string['your_current_role'] = 'Your current role';
$string['your_current_roles'] = 'Your current roles';

$string['you_cannot_update_rule_with_affected_users'] = 'You cannot update rule, which has affected user at the moment';
