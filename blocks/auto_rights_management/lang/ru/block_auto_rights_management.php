<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    block
 * @subpackage auto_rights_management
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2018 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Блок автоматического управления правами';
$string['auto_rights_management'] = 'Автоматическое управление правами';

$string['auto_rights_management:addinstance'] = 'Добавить новый блок автоматического управления правами';
$string['auto_rights_management:viewforteacher'] = 'Видеть блок автоматического управления правами';
$string['auto_rights_management:managerules'] = 'Управлять правилами';
$string['auto_rights_management:manageusers'] = 'Управлять пользователями';

$string['rule_form_rule_name'] = 'Название правила';
$string['rule_form_rule_name_help'] = 'Например, "Ограничить коммуникацию во время экзамена".';
$string['rule_form_rule_name_required'] = 'Укажите название правила';
$string['rule_form_rule_name_not_unique'] = 'Название правила уже занято';

$string['rule_form_context'] = 'Контекст';
$string['rule_form_context_help'] = 'В каком контексте данное правило должно работать.';
$string['rule_form_context_required'] = 'Выберите контекст';
$string['rule_form_context_not_exist'] = 'Выбранный вами контекст не существует';

$string['rule_form_event_name'] = 'Событие';
$string['rule_form_event_name_help'] = 'После какого события данное правило должно сработать.';
$string['rule_form_event_name_required'] = 'Выберите событие';
$string['rule_form_event_name_not_exist'] = 'Выбранное событие не существует';
$string['rule_form_event_name_quiz_started'] = 'Начат тест';
$string['rule_form_event_name_poas_task_started'] = 'Получено poasassignment задание';
$string['rule_form_event_name_supervised_session_started'] = 'Начата supervised сессия';

$string['rule_form_event_contexts'] = 'Контекст события';
$string['rule_form_event_contexts_help'] = 'В каком контексте должно сработать событие.';
$string['rule_form_event_contexts_not_exists'] = 'Выбранный контекст не существует для данного события.';
$string['you_have_not_existing_context_saved_to_db'] = 'Каким-то образом в базе данных сохранен не существующий контекст (возможно, в следствие обновления Moodle)';

$string['rule_form_capabilities_to_allow'] = 'Права, которые нужно разрешить';
$string['rule_form_capabilities_to_allow_help'] = 'Какие права нужно разрешить.';
$string['rule_form_capabilities_required'] = 'Выберите права, которые нужно разрешить или запретить';
$string['rule_form_capabilities_cannot_allow_and_disallow_the_same_capabilities'] = 'Нельзя одновременно разрешить и запретить одни и теже права';
$string['rule_form_capabilities_not_exist'] = 'Выбранные вами права не существуют';

$string['you_have_not_existing_capabilities_saved_to_db'] = 'Каким-то образом в базе данных сохранены не существующие права (возможно, в следствие обновления Moodle)';

$string['rule_form_capabilities_to_disallow'] = 'Права, которые нужно запретить';
$string['rule_form_capabilities_to_disallow_help'] = 'Какие права нужно запретить.';

$string['rule_form_roles_to_affect'] = 'Применять на пользователей с ролью';
$string['rule_form_roles_to_affect_help'] = 'Правило будет применено только на пользователей с указанными ролями.';
$string['rule_form_roles_to_affect_required'] = 'Выберите роли пользователей';
$string['rule_form_roles_to_affect_not_exists'] = 'Одна из указанных вами ролей не существует';
$string['you_have_not_existing_roles_to_affect_saved_to_db'] = 'Каким-то образом в базе данных сохранены не существующие роли пользователей (возможно, в следствие обновления Moodle)';

$string['rule_form_roles_to_affect_in_context'] = 'Применять на пользователей с ролью в контексте';
$string['rule_form_roles_to_affect_in_context_help'] = 'Правило будет применено только на пользователей, которые имеют указанные выше роли в выбранном контексте.';
$string['rule_form_roles_to_affect_in_context_required'] = 'Выберите контекст ролей';
$string['rule_form_roles_to_affect_in_context_not_exist'] = 'Выбранный вами контекст не существует';

$string['rule_form_role_name'] = 'Название роли, присваемой пользователю';
$string['rule_form_role_name_help'] = 'Например, "Студент во время экзамена".';
$string['rule_form_role_name_required'] = 'Укажите название роли';
$string['rule_form_role_name_not_unique'] = 'Название роли уже занято';

$string['list_of_rules'] = 'Список правил';
$string['list_of_affected_users'] = 'Список пользователей, затронутых правилами';

$string['rules'] = 'Правила';
$string['rule'] = 'Правило';
$string['delete_rule'] = 'Удаление правила';
$string['delete_rule_confirm'] = 'Вы уверены, что хотите удалить правило? Все права пользователей, затронутых данным правилом будут восстановлены.';
$string['actions'] = 'Действия';
$string['edit'] = 'Редактировать';
$string['delete'] = 'Удалить';
$string['create'] = 'Добавить';
$string['create_rule'] = 'Добавление правила';
$string['edit_rule'] = 'Редактирование правила';
$string['save'] = 'Сохранить';
$string['users'] = 'Пользователи';
$string['user'] = 'Пользователь';
$string['restore_rights'] = 'Восстановить прежние права';

$string['you_are_not_affected'] = 'Вам не назначена никакая дополнительная роль на данный момент';
$string['your_current_role'] = 'Ваша текущая роль';
$string['your_current_roles'] = 'Ваши текущие роли';

$string['you_cannot_update_rule_with_affected_users'] = 'Вы не можете обновить правило, которое имеет затронутых пользователей на данный момент';
