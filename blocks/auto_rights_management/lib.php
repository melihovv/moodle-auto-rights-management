<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

use block_auto_rights_management\rights_manager;
use block_auto_rights_management\rule_dto;

defined('MOODLE_INTERNAL') || die();

require_once __DIR__ . '/crud/rules/rule_form.php';

/**
 * Class auto_rights_management_events_handler.
 *
 * @package    block
 * @subpackage auto_rights_management
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2018 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class auto_rights_management_events_handler {
    /** @var array */
    private static $finishtostarteventmap = [
        rule_form::EVENT_QUIZ_ATTEMPT_SUBMITTED => rule_form::EVENT_QUIZ_ATTEMPT_STARTED,
        rule_form::EVENT_QUIZ_ATTEMPT_BECAMEOVERDUE => rule_form::EVENT_QUIZ_ATTEMPT_STARTED,
        rule_form::EVENT_POAS_ASSESSABLE_SUBMITTED => rule_form::EVENT_POAS_TASK_SELECTED,
        rule_form::EVENT_SUPERVISED_FINISH_SESSION => rule_form::EVENT_SUPERVISED_START_SESSION,
        rule_form::EVENT_SUPERVISED_DELETE_SESSION => rule_form::EVENT_SUPERVISED_START_SESSION,
    ];

    /** @var array */
    private static $similareventmap = [
        rule_form::EVENT_SUPERVISED_START_PLANNED_SESSION => rule_form::EVENT_SUPERVISED_START_SESSION,
    ];

    /**
     * @param \core\event\base $event
     * @throws dml_exception
     * @throws coding_exception
     */
    public static function handle(\core\event\base $event) {
        global $DB;

        $context = $event->get_context();

        // Block instance in course where event is occurred.
        $instance = $DB->get_record('block_instances', [
            'blockname' => 'auto_rights_management',
            'parentcontextid' => $context->contextlevel == CONTEXT_COURSE
                ? $context->id
                : $context->get_parent_context()->id,
        ]);

        $eventclass = get_class($event);
        $rollback = false;

        if (isset(self::$finishtostarteventmap[$eventclass])) {
            $eventclass = self::$finishtostarteventmap[$eventclass];
            $rollback = true;
        }

        if (isset(self::$similareventmap[$eventclass])) {
            $eventclass = self::$similareventmap[$eventclass];
        }

        $rules = $DB->get_records('block_arm_rules', [
            'blockid' => $instance->id,
            'eventname' => $eventclass,
        ]);

        $userids = [$event->userid];

        if ($eventclass === rule_form::EVENT_SUPERVISED_START_SESSION
            && isset($event->get_data()['other']['groupid'])
        ) {
            global $COURSE;

            $users = [];
            $groupid = $event->get_data()['other']['groupid'];

            if ($groupid == 0) {
                $groups = groups_get_all_groups($COURSE->id);

                foreach ($groups as $group) {
                    $users = $users + groups_get_members($group->id);
                }
            } else {
                $users = groups_get_members($groupid);
            }

            $userids = array_map(function ($user) {
                return $user->id;
            }, $users);
        }

        foreach ($userids as $userid) {
            foreach ($rules as $rule) {
                $dto = rule_dto::from_rule($rule, $userid);
                $eventcontexts = $dto->get_event_contexts();

                if ($eventcontexts) {
                    $contextid = $event->contextid;

                    if ($eventclass === rule_form::EVENT_SUPERVISED_START_SESSION
                        && isset($event->get_data()['other']['lessontypeid'])
                    ) {
                        $contextid = $event->get_data()['other']['lessontypeid'];
                    }

                    if (! in_array($contextid, $eventcontexts)) {
                        continue;
                    }
                }

                if ($rollback) {
                    rights_manager::create($rule->blockid)->rollback($dto);
                } else {
                    rights_manager::create($rule->blockid)->change_access($dto);
                }
            }
        }
    }
}

