@block @block_auto_rights_management
Feature: Several rules apply in row
  Background:
    Given the following "users" exist:
      | username | firstname | lastname | email                |
      | teacher1 | Terry1    | Teacher1 | teacher1@example.com |
      | student1 | Sam1      | Student1 | student1@example.com |
    And the following "courses" exist:
      | fullname | shortname | category |
      | Course 1 | C1        | 0        |
    And the following "course enrolments" exist:
      | user | course | role           |
      | teacher1 | C1 | editingteacher |
      | student1 | C1 | student        |
    And the following "activities" exist:
      | activity   | name           | course | idnumber |
      | chat       | Chat1          | C1     | chat1    |
      | quiz       | Quiz1          | C1     | quiz1    |
    And the following "groups" exist:
      | name    | course | idnumber |
      | Group 1 | C1     | G1       |
    Given the following "group members" exist:
      | user     | group |
      | student1 | G1    |
    And I log in as "teacher1"
    And I am on "Course 1" course homepage with editing mode on
    And I add the "Auto rights management block" block if not present
    And I add a "True/False" question to the "Quiz1" quiz with:
      | Question name                      | First question                          |
      | Question text                      | Answer the first question               |
      | General feedback                   | Thank you, this is the general feedback |
      | Correct answer                     | False                                   |
      | Feedback for the response 'True'.  | So you think it is true                 |
      | Feedback for the response 'False'. | So you think it is false                |
    And I am on "Course 1" course homepage
    And I add the "Supervised" block if not present
    And I follow "Classrooms"
    And I press "Add a classroom ..."
    And I set the field "name" to "902"
    And I set the field "iplist" to "127.0.0.1"
    And I press "Save changes"

    And I am on "Course 1" course homepage
    And I follow "Rules"
    And I follow "Create"
    And I set the field "rule_form_rule_name" to "Student during exam without chat"
    # Fill rights to disallow
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:chat" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:view" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I select "Course: Course 1" from the "rule_form_context" singleselect
    And I select "Quiz is started" from the "rule_form_event_name" singleselect
    And I select "student" from the "rule_form_roles_to_affect[]" singleselect
    And I select "Course: Course 1" from the "rule_form_roles_to_affect_in_context" singleselect
    And I set the field "rule_form_role_name" to "student-during-exam-without-chat"
    And I press "Save"

    And I am on "Course 1" course homepage
    And I follow "Rules"
    And I follow "Create"
    And I set the field "rule_form_rule_name" to "Student during exam with extra rights"
    # Fill rights to allow
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_allow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "block/auto_rights_management:viewforteacher" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_allow')]/following-sibling::"
    And I select "Course: Course 1" from the "rule_form_context" singleselect
    And I select "Supervised session is started" from the "rule_form_event_name" singleselect
    And I select "student" from the "rule_form_roles_to_affect[]" singleselect
    And I select "Course: Course 1" from the "rule_form_roles_to_affect_in_context" singleselect
    And I set the field "rule_form_role_name" to "student-during-exam-with-extra-rights"
    And I press "Save"

    And I am on "Course 1" course homepage
    And I follow "Affected users"
    And I should see "Nothing to display"
    And I log out

    And I log in as "student1"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I should see "You does not have any extra role at the moment"
    And I should not see "Rules"
    And I should not see "Affected users"
    And I follow "Quiz1"
    When I press "Attempt quiz now"
    And I am on "Course 1" course homepage
    Then I should not see "Chat1"
    And I should see "Your current role: student-during-exam-without-chat"
    And I log out

    And I log in as "teacher1"
    And I am on "Course 1" course homepage
    And I follow "Affected users"
    And I should not see "Nothing to display"
    And I should see "Student1"
    And I am on "Course 1" course homepage
    And I select "Group 1" from the "groupid" singleselect
    And I press "Start session"
    And I log out

    And I log in as "student1"
    And I am on "Course 1" course homepage
    And I should not see "Chat1"
    And I should see "Rules"
    And I should see "Affected users"

  @javascript
  Scenario: Student rights are changed two times in a row and are restored in order in which they were created
    And I am on "Course 1" course homepage
    And I follow "Quiz1"
    And I press "Continue the last attempt"
    And I set the field "True" to "1"
    And I press "Finish attempt ..."
    And I press "Submit all and finish"
    And I click on "Submit all and finish" "button" in the "Confirmation" "dialogue"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I should see "Rules"
    And I should see "Affected users"
    And I log out

    And I log in as "teacher1"
    And I am on "Course 1" course homepage
    And I follow "Affected users"
    And I should see "Student1"
    And I am on "Course 1" course homepage
    And I press "Finish session"
    And I follow "Affected users"
    And I should see "Nothing to display"
    And I log out

    And I log in as "student1"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I should not see "Rules"
    And I should not see "Affected users"
    And I should see "You does not have any extra role at the moment"

  @javascript
  Scenario: Student rights are changed two times in a row and are restored in reverse order in which they were created
    And I log out
    And I log in as "teacher1"
    And I am on "Course 1" course homepage
    And I follow "Affected users"
    And I should see "Student1"
    And I am on "Course 1" course homepage
    And I press "Finish session"
    And I follow "Affected users"
    And I should see "Student1"
    And I log out

    And I log in as "student1"
    And I am on "Course 1" course homepage
    And I should not see "Chat1"
    And I should not see "Rules"
    And I should not see "Affected users"
    And I should see "Your current role: student-during-exam-without-chat"

    And I am on "Course 1" course homepage
    And I follow "Quiz1"
    And I press "Continue the last attempt"
    And I set the field "True" to "1"
    And I press "Finish attempt ..."
    And I press "Submit all and finish"
    And I click on "Submit all and finish" "button" in the "Confirmation" "dialogue"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I should not see "Rules"
    And I should not see "Affected users"
