<?php

require_once __DIR__ . '/../../../../lib/behat/behat_base.php';

class behat_block_auto_rights_management extends behat_base {
    /**
     * Select item from autocomplete list.
     *
     * @Given /^I click on "([^"]*)" item in the autocomplete list in "([^"]*)"$/
     *
     * @param string $item
     */
    public function i_click_on_item_in_the_autocomplete_list($item, $prefix) {
        $xpathtarget = "{$prefix}ul[@class='form-autocomplete-suggestions']//li[contains(.,'$item')]";

        $this->execute('behat_general::i_click_on', [$xpathtarget, 'xpath_element']);
    }

    /**
     * Add poasassignment task to database.
     *
     * @Given /^the following poasassignment task exists "([^"]*)" "([^"]*)"$/
     *
     * @param string $item
     */
    public function the_following_poasassignment_task_exists($taskname, $poasassignmentname) {
        global $DB;

        $records = $DB->get_records('poasassignment', [], 'id DESC', '*', 0, 1);
        $poasassignment = $DB->get_record('poasassignment', ['id' => reset($records)->id]);

        $DB->insert_record('poasassignment_tasks', (object)[
            'poasassignmentid' => $poasassignment->id,
            'name' => $taskname,
            'description' => 'Description',
            'deadline' => strtotime('+1 year'),
            'hidden' => 0,
        ]);

        // Disable file answer type for this poasassignment instance.
        $DB->delete_records('poasassignment_ans_stngs', [
            'poasassignmentid' => $poasassignment->id,
            'answerid' => 1,
        ]);

        $record = $DB->get_record('poasassignment_ans_stngs', [
            'poasassignmentid' => $poasassignment->id,
            'answerid' => 2,
        ]);

        if (! $record) {
            $DB->insert_record('poasassignment_ans_stngs', [
                'poasassignmentid' => $poasassignment->id,
                'answerid' => 2,
            ]);
        }

        // Update poasassignment name
        $poasassignment->name = $poasassignmentname;
        $DB->update_record('poasassignment', $poasassignment);
    }
}
