@block @block_auto_rights_management
Feature: Block auto rights management integration with poas
  In order to automatically manage user rights when he starts poas assignment
  As a teacher
  I can configure rights management block for integration with poas assignment

  Background:
    Given the following "users" exist:
      | username | firstname | lastname | email                |
      | teacher1 | Terry1    | Teacher1 | teacher1@example.com |
      | student1 | Sam1      | Student1 | student1@example.com |
    And the following "courses" exist:
      | fullname | shortname | category |
      | Course 1 | C1        | 0        |
    And the following "course enrolments" exist:
      | user | course | role           |
      | teacher1 | C1 | editingteacher |
      | student1 | C1 | student        |
    And the following "activities" exist:
      | activity   | name           | course | idnumber |
      | chat       | Chat1          | C1     | chat1    |
    And the following "activities" exist:
      | activity       | course | idnumber | name                | intro                     | activateindividualtasks | taskgiverid | uniqueness | answerfile | answertext | maxfilesize | fileamount    | fileextensions    |
      | poasassignment | C1     | poas1    | Poasassignment Test | Test Poasassignment intro | 1                       | 3           | 0          | 0          | 1          | 0           | -1            |                   |
    And the following poasassignment task exists "Task1" "Poasassignment Test"
    And the following "activities" exist:
      | activity       | course | idnumber | name                  | intro                       | activateindividualtasks | taskgiverid | uniqueness | answerfile | answertext | maxfilesize | fileamount    | fileextensions    |
      | poasassignment | C1     | poas2    | Poasassignment Test 2 | Test Poasassignment intro 2 | 1                       | 3           | 0          | 0          | 1          | 0           | -1            |                   |
    And the following poasassignment task exists "Task2" "Poasassignment Test 2"
    And I log in as "teacher1"
    And I am on "Course 1" course homepage with editing mode on
    And I add the "Auto rights management block" block if not present

  @javascript
  Scenario: Student rights are changed when he starts a poas assignment
    And I am on "Course 1" course homepage
    And I follow "Rules"
    And I follow "Create"
    And I set the field "rule_form_rule_name" to "Student during exam"
    # Fill rights to disallow
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:chat" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:view" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I select "Course: Course 1" from the "rule_form_context" singleselect
    And I select "Poasassignment task is started" from the "rule_form_event_name" singleselect
    And I select "student" from the "rule_form_roles_to_affect[]" singleselect
    And I select "Course: Course 1" from the "rule_form_roles_to_affect_in_context" singleselect
    And I set the field "rule_form_role_name" to "student-during-exam"
    And I press "Save"
    And I am on "Course 1" course homepage
    And I follow "Affected users"
    And I should see "Nothing to display"
    And I log out

    And I log in as "student1"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I should see "You does not have any extra role at the moment"
    # Click on "Poasassignment Test"
    And I click on ".modtype_poasassignment:nth-of-type(2) a" "css_element"
    And I follow "Click here to take task"
    And I am on "Course 1" course homepage
    Then I should not see "Chat1"
    And I should see "Your current role: student-during-exam"
    And I log out

    And I log in as "teacher1"
    And I am on "Course 1" course homepage
    And I follow "Affected users"
    And I should not see "Nothing to display"
    And I should see "Student1"
    And I log out

    And I log in as "student1"
    And I am on "Course 1" course homepage
    # Click on "Poasassignment Test", 1 because chat is hidden
    And I click on ".modtype_poasassignment:nth-of-type(1) a" "css_element"
    And I press "Add submission"
    And I set the field "text_editor" to "my submission"
    And I press "Send submission"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I should see "You does not have any extra role at the moment"
    And I log out

    And I log in as "teacher1"
    And I am on "Course 1" course homepage
    And I follow "Affected users"
    And I should see "Nothing to display"

  @javascript
  Scenario: Student rights are changed when he starts specified in rule poas assignment and remain the same then he starts not specified one
    And I am on "Course 1" course homepage
    And I follow "Rules"
    And I follow "Create"
    And I set the field "rule_form_rule_name" to "Student during exam"
    # Fill rights to disallow
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:chat" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:view" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I select "Course: Course 1" from the "rule_form_context" singleselect
    And I select "Poasassignment task is started" from the "rule_form_event_name" singleselect
    And I select "Poasassignment Test" from the "rule_form_event_contexts_poasassignment[]" singleselect
    And I select "student" from the "rule_form_roles_to_affect[]" singleselect
    And I select "Course: Course 1" from the "rule_form_roles_to_affect_in_context" singleselect
    And I set the field "rule_form_role_name" to "student-during-exam"
    And I press "Save"
    And I am on "Course 1" course homepage
    And I follow "Affected users"
    And I should see "Nothing to display"
    And I log out

    And I log in as "student1"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I should see "You does not have any extra role at the moment"
    # Click on "Poasassignment Test"
    And I click on ".modtype_poasassignment:nth-of-type(2) a" "css_element"
    And I follow "Click here to take task"
    And I am on "Course 1" course homepage
    Then I should not see "Chat1"
    And I should see "Your current role: student-during-exam"

    # Click on "Poasassignment Test", 1 because chat is hidden
    And I click on ".modtype_poasassignment:nth-of-type(1) a" "css_element"
    And I press "Add submission"
    And I set the field "text_editor" to "my submission"
    And I press "Send submission"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I should see "You does not have any extra role at the moment"

    # Click on "Poasassignment Test 2"
    And I click on ".modtype_poasassignment:nth-of-type(3) a" "css_element"
    And I follow "Click here to take task"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I should see "You does not have any extra role at the moment"
    # Click on "Poasassignment Test 2"
    And I click on ".modtype_poasassignment:nth-of-type(3) a" "css_element"
    And I press "Add submission"
    And I set the field "text_editor" to "my submission"
    And I press "Send submission"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I should see "You does not have any extra role at the moment"
