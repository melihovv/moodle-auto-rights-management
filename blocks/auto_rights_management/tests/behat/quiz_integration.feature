@block @block_auto_rights_management
Feature: Block auto rights management integration with quiz
  In order to automatically manage user rights when he starts quiz
  As a teacher
  I can configure rights management block for integration with quiz

  Background:
    Given the following "users" exist:
      | username | firstname | lastname | email                |
      | teacher1 | Terry1    | Teacher1 | teacher1@example.com |
      | student1 | Sam1      | Student1 | student1@example.com |
    And the following "courses" exist:
      | fullname | shortname | category |
      | Course 1 | C1        | 0        |
      | Course 2 | C2        | 0        |
    And the following "course enrolments" exist:
      | user | course | role           |
      | teacher1 | C1 | editingteacher |
      | student1 | C1 | student        |
      | student1 | C2 | student        |
    And the following "activities" exist:
      | activity   | name           | course | idnumber |
      | chat       | Chat1          | C1     | chat1    |
      | chat       | Chat2          | C2     | chat2    |
      | quiz       | Quiz1          | C1     | quiz1    |
      | quiz       | Quiz2          | C1     | quiz2    |
    And I log in as "teacher1"
    And I am on "Course 1" course homepage with editing mode on
    And I add the "Auto rights management block" block if not present
    And I add a "True/False" question to the "Quiz1" quiz with:
      | Question name                      | First question                          |
      | Question text                      | Answer the first question               |
      | General feedback                   | Thank you, this is the general feedback |
      | Correct answer                     | False                                   |
      | Feedback for the response 'True'.  | So you think it is true                 |
      | Feedback for the response 'False'. | So you think it is false                |
    And I am on "Course 1" course homepage
    And I add a "True/False" question to the "Quiz2" quiz with:
      | Question name                      | First question                          |
      | Question text                      | Answer the first question               |
      | General feedback                   | Thank you, this is the general feedback |
      | Correct answer                     | False                                   |
      | Feedback for the response 'True'.  | So you think it is true                 |
      | Feedback for the response 'False'. | So you think it is false                |

  @javascript
  Scenario: Student rights are changed when he starts a quiz
    And I am on "Course 1" course homepage
    And I follow "Rules"
    And I follow "Create"
    And I set the field "rule_form_rule_name" to "Student during exam"
    # Fill rights to disallow
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:chat" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:view" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I select "Course: Course 1" from the "rule_form_context" singleselect
    And I select "Quiz is started" from the "rule_form_event_name" singleselect
    And I select "student" from the "rule_form_roles_to_affect[]" singleselect
    And I select "Course: Course 1" from the "rule_form_roles_to_affect_in_context" singleselect
    And I set the field "rule_form_role_name" to "student-during-exam"
    And I press "Save"
    And I am on "Course 1" course homepage
    And I follow "Affected users"
    And I should see "Nothing to display"
    And I log out

    And I log in as "student1"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I should see "You does not have any extra role at the moment"
    And I follow "Quiz1"
    When I press "Attempt quiz now"
    And I am on "Course 1" course homepage
    Then I should not see "Chat1"
    And I should see "Your current role: student-during-exam"
    And I log out

    And I log in as "teacher1"
    And I am on "Course 1" course homepage
    And I follow "Affected users"
    And I should not see "Nothing to display"
    And I should see "Student1"
    And I log out

    And I log in as "student1"
    And I am on "Course 1" course homepage
    And I follow "Quiz1"
    And I press "Continue the last attempt"
    And I set the field "True" to "1"
    And I press "Finish attempt ..."
    And I press "Submit all and finish"
    And I click on "Submit all and finish" "button" in the "Confirmation" "dialogue"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I should see "You does not have any extra role at the moment"
    And I log out

    And I log in as "teacher1"
    And I am on "Course 1" course homepage
    And I follow "Affected users"
    And I should see "Nothing to display"

  @javascript
  Scenario: Student rights are changed when he starts specified in rule quiz and remain the same then he starts not specified one
    And I am on "Course 1" course homepage
    And I follow "Rules"
    And I follow "Create"
    And I set the field "rule_form_rule_name" to "Student during exam"
    # Fill rights to disallow
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:chat" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:view" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I select "Course: Course 1" from the "rule_form_context" singleselect
    And I select "Quiz is started" from the "rule_form_event_name" singleselect
    And I select "Quiz1" from the "rule_form_event_contexts_quiz[]" singleselect
    And I select "student" from the "rule_form_roles_to_affect[]" singleselect
    And I select "Course: Course 1" from the "rule_form_roles_to_affect_in_context" singleselect
    And I set the field "rule_form_role_name" to "student-during-exam"
    And I press "Save"
    And I am on "Course 1" course homepage
    And I follow "Affected users"
    And I should see "Nothing to display"
    And I log out

    And I log in as "student1"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I follow "Quiz1"
    When I press "Attempt quiz now"
    And I am on "Course 1" course homepage
    Then I should not see "Chat1"
    And I follow "Quiz1"
    And I press "Continue the last attempt"
    And I set the field "True" to "1"
    And I press "Finish attempt ..."
    And I press "Submit all and finish"
    And I click on "Submit all and finish" "button" in the "Confirmation" "dialogue"
    And I am on "Course 1" course homepage
    And I should see "Chat1"

    And I follow "Quiz2"
    When I press "Attempt quiz now"
    And I am on "Course 1" course homepage
    Then I should see "Chat1"
    And I follow "Quiz2"
    And I press "Continue the last attempt"
    And I set the field "True" to "1"
    And I press "Finish attempt ..."
    And I press "Submit all and finish"
    And I click on "Submit all and finish" "button" in the "Confirmation" "dialogue"
    And I am on "Course 1" course homepage
    And I should see "Chat1"

  @javascript
  Scenario: Student rights remain the same in another course
    And I am on "Course 1" course homepage
    And I follow "Rules"
    And I follow "Create"
    And I set the field "rule_form_rule_name" to "Student during exam"
    # Fill rights to disallow
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:chat" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:view" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I select "Course: Course 1" from the "rule_form_context" singleselect
    And I select "Quiz is started" from the "rule_form_event_name" singleselect
    And I select "student" from the "rule_form_roles_to_affect[]" singleselect
    And I select "Course: Course 1" from the "rule_form_roles_to_affect_in_context" singleselect
    And I set the field "rule_form_role_name" to "student-during-exam"
    And I press "Save"
    And I am on "Course 1" course homepage
    And I follow "Affected users"
    And I should see "Nothing to display"
    And I log out

    And I log in as "student1"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I should see "You does not have any extra role at the moment"
    And I follow "Quiz1"
    And I press "Attempt quiz now"
    And I am on "Course 1" course homepage
    And I should not see "Chat1"
    And I should see "Your current role: student-during-exam"
    And I am on "Course 2" course homepage
    And I should see "Chat2"
