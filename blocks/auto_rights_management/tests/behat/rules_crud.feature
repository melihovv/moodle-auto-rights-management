@block @block_auto_rights_management
Feature: Block auto rights management rule crud
  In order to automatically manage user rights
  As a teacher
  I can create/read/update/delete rules

  Background:
    Given the following "users" exist:
      | username | firstname | lastname | email                |
      | teacher1 | Terry1    | Teacher1 | teacher1@example.com |
      | student1 | Sam1      | Student1 | student1@example.com |
    And the following "courses" exist:
      | fullname | shortname | category |
      | Course 1 | C1        | 0        |
    And the following "course enrolments" exist:
      | user | course | role           |
      | teacher1 | C1 | editingteacher |
      | student1 | C1 | student        |
    And the following "activities" exist:
      | activity   | name           | course | idnumber |
      | chat       | Chat1          | C1     | chat1    |
      | quiz       | Quiz1          | C1     | quiz1    |
    And I log in as "teacher1"
    And I am on "Course 1" course homepage with editing mode on
    And I add the "Auto rights management block" block if not present

  @javascript
  Scenario: Teacher cannot create rule with the same allow and dissalow rights
    And I am on "Course 1" course homepage
    And I follow "Rules"
    And I follow "Create"
    # Fill rights to allow
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_allow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:chat" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_allow')]/following-sibling::"
    # Fill rights to disallow
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:chat" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I press "Save"
    And I should see "Cannot simultaneously allow and disallow the same rights."

  @javascript
  Scenario: Teacher cannot create rule with existing role name
    And I am on "Course 1" course homepage
    And I follow "Rules"
    And I follow "Create"
    And I set the field "rule_form_rule_name" to "Student during exam"
    # Fill rights to disallow
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:chat" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:view" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I select "Course: Course 1" from the "rule_form_context" singleselect
    And I select "Quiz is started" from the "rule_form_event_name" singleselect
    And I select "student" from the "rule_form_roles_to_affect[]" singleselect
    And I select "Course: Course 1" from the "rule_form_roles_to_affect_in_context" singleselect
    And I set the field "rule_form_role_name" to "student-during-exam"
    And I press "Save"
    And I follow "Create"
    And I set the field "rule_form_role_name" to "student-during-exam"
    And I press "Save"
    And I should see "Role name is already taken"

  @javascript
  Scenario: Teacher cannot update rule with currently affected users
    And I add a "True/False" question to the "Quiz1" quiz with:
      | Question name                      | First question                          |
      | Question text                      | Answer the first question               |
      | General feedback                   | Thank you, this is the general feedback |
      | Correct answer                     | False                                   |
      | Feedback for the response 'True'.  | So you think it is true                 |
      | Feedback for the response 'False'. | So you think it is false                |
    And I am on "Course 1" course homepage
    And I follow "Rules"
    And I follow "Create"
    And I set the field "rule_form_rule_name" to "Student during exam"
    # Fill rights to disallow
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:chat" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:view" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I select "Course: Course 1" from the "rule_form_context" singleselect
    And I select "Quiz is started" from the "rule_form_event_name" singleselect
    And I select "student" from the "rule_form_roles_to_affect[]" singleselect
    And I select "Course: Course 1" from the "rule_form_roles_to_affect_in_context" singleselect
    And I set the field "rule_form_role_name" to "student-during-exam"
    And I press "Save"
    And I am on "Course 1" course homepage
    And I log out

    And I log in as "student1"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I follow "Quiz1"
    And I press "Attempt quiz now"
    And I am on "Course 1" course homepage
    And I should not see "Chat1"
    And I log out

    And I log in as "teacher1"
    And I am on "Course 1" course homepage
    And I follow "Rules"
    # Click edit rule icon
    And I click on "Edit" "icon"
    And I set the field "rule_form_rule_name" to "Student during exam 2"
    And I press "Save"
    And I should see "You cannot update rule, which has affected user at the moment"

  @javascript
  Scenario: Teacher cannot update rule with currently affected users
    And I add a "True/False" question to the "Quiz1" quiz with:
      | Question name                      | First question                          |
      | Question text                      | Answer the first question               |
      | General feedback                   | Thank you, this is the general feedback |
      | Correct answer                     | False                                   |
      | Feedback for the response 'True'.  | So you think it is true                 |
      | Feedback for the response 'False'. | So you think it is false                |
    And I am on "Course 1" course homepage
    And I follow "Rules"
    And I follow "Create"
    And I set the field "rule_form_rule_name" to "Student during exam"
    # Fill rights to disallow
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:chat" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:view" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I select "Course: Course 1" from the "rule_form_context" singleselect
    And I select "Quiz is started" from the "rule_form_event_name" singleselect
    And I select "student" from the "rule_form_roles_to_affect[]" singleselect
    And I select "Course: Course 1" from the "rule_form_roles_to_affect_in_context" singleselect
    And I set the field "rule_form_role_name" to "student-during-exam"
    And I press "Save"
    And I am on "Course 1" course homepage
    And I log out

    And I log in as "student1"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I follow "Quiz1"
    And I press "Attempt quiz now"
    And I am on "Course 1" course homepage
    And I should not see "Chat1"
    And I log out

    And I log in as "teacher1"
    And I am on "Course 1" course homepage
    And I follow "Affected users"
    And I should see "Student1"
    And I am on "Course 1" course homepage
    And I follow "Rules"
    # Click delete rule icon
    And I click on "Delete" "icon"
    And I press "Continue"
    And I am on "Course 1" course homepage
    And I follow "Affected users"
    And I should see "Nothing to display"
