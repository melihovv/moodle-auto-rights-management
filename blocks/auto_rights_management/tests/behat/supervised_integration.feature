@block @block_auto_rights_management
Feature: Block auto rights management integration with supervised
  In order to automatically manage user rights when teacher starts supervised session
  As a teacher
  I can configure rights management block for integration with supervised

  Background:
    Given the following "users" exist:
      | username | firstname | lastname | email                |
      | teacher1 | Terry1    | Teacher1 | teacher1@example.com |
      | student1 | Sam1      | Student1 | student1@example.com |
    And the following "courses" exist:
      | fullname | shortname | category |
      | Course 1 | C1        | 0        |
    And the following "course enrolments" exist:
      | user     | course | role           |
      | teacher1 | C1     | editingteacher |
      | student1 | C1     | student        |
    And the following "activities" exist:
      | activity   | name           | course | idnumber |
      | chat       | Chat1          | C1     | chat1    |
    And the following "groups" exist:
      | name    | course | idnumber |
      | Group 1 | C1     | G1       |
    Given the following "group members" exist:
      | user     | group |
      | student1 | G1    |
    And I log in as "teacher1"
    And I am on "Course 1" course homepage with editing mode on
    And I add the "Auto rights management block" block if not present
    And I add the "Supervised" block if not present
    And I follow "Classrooms"
    And I press "Add a classroom ..."
    And I set the field "name" to "902"
    And I set the field "iplist" to "127.0.0.1"
    And I press "Save changes"
    And I am on "Course 1" course homepage
    And I follow "Lesson types"
    And I press "Add a lesson type ..."
    And I set the field "name" to "LessonType1"
    And I press "Save changes"
    And I am on "Course 1" course homepage
    And I follow "Lesson types"
    And I press "Add a lesson type ..."
    And I set the field "name" to "LessonType2"
    And I press "Save changes"
    And I am on "Course 1" course homepage

  @javascript
  Scenario: Student rights are changed when teacher starts supervised session
    And I am on "Course 1" course homepage
    And I follow "Rules"
    And I follow "Create"
    And I set the field "rule_form_rule_name" to "Student during exam"
    # Fill rights to disallow
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:chat" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:view" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I select "Course: Course 1" from the "rule_form_context" singleselect
    And I select "Supervised session is started" from the "rule_form_event_name" singleselect
    And I select "student" from the "rule_form_roles_to_affect[]" singleselect
    And I select "Course: Course 1" from the "rule_form_roles_to_affect_in_context" singleselect
    And I set the field "rule_form_role_name" to "student-during-exam"
    And I press "Save"
    And I am on "Course 1" course homepage
    And I follow "Affected users"
    And I should see "Nothing to display"
    And I log out

    And I log in as "student1"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I should see "You does not have any extra role at the moment"
    And I log out

    And I log in as "teacher1"
    And I am on "Course 1" course homepage
    And I select "Group 1" from the "groupid" singleselect
    And I press "Start session"
    And I log out

    And I log in as "student1"
    And I am on "Course 1" course homepage
    And I should not see "Chat1"
    And I should see "Your current role: student-during-exam"
    And I log out

    And I log in as "teacher1"
    And I am on "Course 1" course homepage
    And I follow "Affected users"
    And I should not see "Nothing to display"
    And I should see "Student1"
    And I am on "Course 1" course homepage
    And I press "Finish session"
    And I log out

    And I log in as "student1"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I should see "You does not have any extra role at the moment"

  @javascript
  Scenario: Student rights are changed when teacher starts supervised session with specified in rule lessontype and remain the same then teacher starts with not specified one
    And I am on "Course 1" course homepage
    And I follow "Rules"
    And I follow "Create"
    And I set the field "rule_form_rule_name" to "Student during exam"
    # Fill rights to disallow
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:chat" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I click on "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::span[contains(@class, 'form-autocomplete-downarrow')]" "xpath_element"
    And I click on "mod/chat:view" item in the autocomplete list in "//select[contains(@id, 'id_rule_form_capabilities_to_disallow')]/following-sibling::"
    And I select "Course: Course 1" from the "rule_form_context" singleselect
    And I select "Supervised session is started" from the "rule_form_event_name" singleselect
    And I select "LessonType1" from the "rule_form_event_contexts_supervised[]" singleselect
    And I select "student" from the "rule_form_roles_to_affect[]" singleselect
    And I select "Course: Course 1" from the "rule_form_roles_to_affect_in_context" singleselect
    And I set the field "rule_form_role_name" to "student-during-exam"
    And I press "Save"
    And I am on "Course 1" course homepage
    And I follow "Affected users"
    And I should see "Nothing to display"
    And I log out

    And I log in as "student1"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I should see "You does not have any extra role at the moment"
    And I log out

    And I log in as "teacher1"
    And I am on "Course 1" course homepage
    And I select "Group 1" from the "groupid" singleselect
    And I select "LessonType1" from the "lessontypeid" singleselect
    And I press "Start session"
    And I log out

    And I log in as "student1"
    And I am on "Course 1" course homepage
    And I should not see "Chat1"
    And I should see "Your current role: student-during-exam"
    And I log out

    And I log in as "teacher1"
    And I am on "Course 1" course homepage
    And I follow "Affected users"
    And I should not see "Nothing to display"
    And I should see "Student1"
    And I am on "Course 1" course homepage
    And I press "Finish session"
    And I log out

    And I log in as "student1"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I should see "You does not have any extra role at the moment"
    And I log out

    And I log in as "teacher1"
    And I am on "Course 1" course homepage
    And I select "Group 1" from the "groupid" singleselect
    And I select "LessonType2" from the "lessontypeid" singleselect
    And I press "Start session"
    And I log out

    And I log in as "student1"
    And I am on "Course 1" course homepage
    And I should see "Chat1"
    And I should see "You does not have any extra role at the moment"
