<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    block
 * @subpackage auto_rights_management
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2018 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

use block_auto_rights_management\affected_users_service;
use block_auto_rights_management\rule_dto;
use block_auto_rights_management\rights_manager;
use block_auto_rights_management\tests\test_helpers;

class rights_manager_test extends advanced_testcase {
    use test_helpers;

    /** @test */
    function it_should_deny_permission_for_student() {
        $this->resetAfterTest();

        list($student, $course) = $this->setup_course_with_student();
        $coursecontext = context_course::instance($course->id);

        $this->assertTrue(has_capability('moodle/grade:view', $coursecontext, $student));

        $dto = rule_dto::create()
            ->set_user_id($student->id)
            ->set_role_name('rolename')
            ->set_capabilities_to_disallow(['moodle/grade:view'])
            ->set_context($coursecontext)
            ->set_rule_id($ruleid = 1);
        rights_manager::create($blockid = 1)->change_access($dto);
        accesslib_clear_all_caches(true);

        $this->assertFalse(has_capability('moodle/grade:view', $coursecontext, $student));
    }

    /** @test */
    function it_should_allow_permission_for_student() {
        $this->resetAfterTest();

        list($student, $course) = $this->setup_course_with_student();
        $coursecontext = context_course::instance($course->id);

        $this->assertFalse(has_capability('moodle/site:configview', $coursecontext, $student));

        $dto = rule_dto::create()
            ->set_user_id($student->id)
            ->set_role_name('rolename')
            ->set_capabilities_to_allow(['moodle/site:configview'])
            ->set_context($coursecontext)
            ->set_rule_id($ruleid = 1);
        rights_manager::create($blockid = 1)->change_access($dto);
        accesslib_clear_all_caches(true);

        $this->assertTrue(has_capability('moodle/site:configview', $coursecontext, $student));
    }

    /** @test */
    function it_should_deny_permission_for_student_and_keep_for_other_one() {
        $this->resetAfterTest();

        list($studentdeny, $course) = $this->setup_course_with_student();
        $studentallow = $this->create_user_and_enrol_him_to_course($course->id);
        $coursecontext = context_course::instance($course->id);

        $this->assertTrue(has_capability('moodle/grade:view', $coursecontext, $studentdeny));
        $this->assertTrue(has_capability('moodle/grade:view', $coursecontext, $studentallow));

        $dto = rule_dto::create()
            ->set_user_id($studentdeny->id)
            ->set_role_name('rolename')
            ->set_capabilities_to_disallow(['moodle/grade:view'])
            ->set_context($coursecontext)
            ->set_rule_id($ruleid = 1);
        rights_manager::create($blockid = 1)->change_access($dto);
        accesslib_clear_all_caches(true);

        $this->assertFalse(has_capability('moodle/grade:view', $coursecontext, $studentdeny));
        $this->assertTrue(has_capability('moodle/grade:view', $coursecontext, $studentallow));
    }

    /** @test */
    function it_should_allow_permission_for_student_and_keep_for_other_one() {
        $this->resetAfterTest();

        list($studentdeny, $course) = $this->setup_course_with_student();
        $studentallow = $this->create_user_and_enrol_him_to_course($course->id);
        $coursecontext = context_course::instance($course->id);

        $this->assertFalse(has_capability('moodle/site:configview', $coursecontext, $studentdeny));
        $this->assertFalse(has_capability('moodle/site:configview', $coursecontext, $studentallow));

        $dto = rule_dto::create()
            ->set_user_id($studentallow->id)
            ->set_role_name('rolename')
            ->set_capabilities_to_allow(['moodle/site:configview'])
            ->set_context($coursecontext)
            ->set_rule_id($ruleid = 1);
        rights_manager::create($blockid = 1)->change_access($dto);
        accesslib_clear_all_caches(true);

        $this->assertFalse(has_capability('moodle/site:configview', $coursecontext, $studentdeny));
        $this->assertTrue(has_capability('moodle/site:configview', $coursecontext, $studentallow));
    }

    /** @test */
    function it_should_deny_permission_for_student_even_if_he_has_allow_permission_in_the_same_context() {
        $this->resetAfterTest();

        list($studentdeny, $course) = $this->setup_course_with_student();
        $coursecontext = context_course::instance($course->id);

        $this->assertFalse(has_capability('moodle/site:configview', $coursecontext, $studentdeny));

        assign_capability('moodle/site:configview', CAP_ALLOW, $studentrole = 5, $coursecontext);
        accesslib_clear_all_caches(true);

        $this->assertTrue(has_capability('moodle/site:configview', $coursecontext, $studentdeny));

        $dto = rule_dto::create()
            ->set_user_id($studentdeny->id)
            ->set_role_name('rolename')
            ->set_capabilities_to_disallow(['moodle/site:configview'])
            ->set_context($coursecontext)
            ->set_rule_id($ruleid = 1);
        rights_manager::create($blockid = 1)->change_access($dto);
        accesslib_clear_all_caches(true);

        $this->assertFalse(has_capability('moodle/site:configview', $coursecontext, $studentdeny));
    }

    /** @test */
    function it_should_deny_permission_for_student_in_one_course_and_allow_in_another() {
        $this->resetAfterTest();

        list($student, $coursedeny) = $this->setup_course_with_student();
        $coursecontextdeny = context_course::instance($coursedeny->id);
        $this->enrol_user($student->id, $coursedeny->id, 'student');

        $courseallow = $this->create_course();
        $coursecontextallow = context_course::instance($courseallow->id);
        $this->enrol_user($student->id, $courseallow->id, 'student');

        $this->assertTrue(has_capability('moodle/grade:view', $coursecontextdeny, $student));
        $this->assertTrue(has_capability('moodle/grade:view', $coursecontextallow, $student));

        $dto = rule_dto::create()
            ->set_user_id($student->id)
            ->set_role_name('rolename')
            ->set_capabilities_to_disallow(['moodle/grade:view'])
            ->set_context($coursecontextdeny)
            ->set_rule_id($ruleid = 1);
        rights_manager::create($blockid = 1)->change_access($dto);
        accesslib_clear_all_caches(true);

        $this->assertFalse(has_capability('moodle/grade:view', $coursecontextdeny, $student));
        $this->assertTrue(has_capability('moodle/grade:view', $coursecontextallow, $student));
    }

    /** @test */
    function it_should_allow_permission_for_student_in_one_course_and_deny_in_another() {
        $this->resetAfterTest();

        list($student, $coursedeny) = $this->setup_course_with_student();
        $coursecontextdeny = context_course::instance($coursedeny->id);
        $this->enrol_user($student->id, $coursedeny->id, 'student');

        $courseallow = $this->create_course();
        $coursecontextallow = context_course::instance($courseallow->id);
        $this->enrol_user($student->id, $courseallow->id, 'student');

        $this->assertFalse(has_capability('moodle/site:configview', $coursecontextdeny, $student));
        $this->assertFalse(has_capability('moodle/site:configview', $coursecontextallow, $student));

        $dto = rule_dto::create()
            ->set_user_id($student->id)
            ->set_role_name('rolename')
            ->set_capabilities_to_allow(['moodle/site:configview'])
            ->set_context($coursecontextallow)
            ->set_rule_id($ruleid = 1);
        rights_manager::create($blockid = 1)->change_access($dto);
        accesslib_clear_all_caches(true);

        $this->assertFalse(has_capability('moodle/site:configview', $coursecontextdeny, $student));
        $this->assertTrue(has_capability('moodle/site:configview', $coursecontextallow, $student));
    }

    /** @test */
    function it_should_deny_permission_for_student_in_the_course_category_and_below_context_and_allow_in_above_contexts() {
        $this->resetAfterTest();

        list($student, $course) = $this->setup_course_with_student();
        $coursecontext = context_course::instance($course->id);
        $categorycontext = $coursecontext->get_parent_context();
        $systemcontext = $categorycontext->get_parent_context();

        $this->assertTrue(has_capability('block/badges:myaddinstance', $coursecontext, $student));
        $this->assertTrue(has_capability('block/badges:myaddinstance', $categorycontext, $student));
        $this->assertTrue(has_capability('block/badges:myaddinstance', $systemcontext, $student));

        $dto = rule_dto::create()
            ->set_user_id($student->id)
            ->set_role_name('rolename')
            ->set_capabilities_to_disallow(['block/badges:myaddinstance'])
            ->set_context($categorycontext)
            ->set_rule_id($ruleid = 1);
        rights_manager::create($blockid = 1)->change_access($dto);
        accesslib_clear_all_caches(true);

        $this->assertFalse(has_capability('block/badges:myaddinstance', $coursecontext, $student));
        $this->assertFalse(has_capability('block/badges:myaddinstance', $categorycontext, $student));
        $this->assertTrue(has_capability('block/badges:myaddinstance', $systemcontext, $student));
    }

    /** @test */
    function it_should_not_allow_permission_for_student_with_prohibit_capability_in_the_same_context() {
        $this->resetAfterTest();

        list($studentdeny, $course) = $this->setup_course_with_student();
        $coursecontext = context_course::instance($course->id);

        $this->assertTrue(has_capability('moodle/grade:view', $coursecontext, $studentdeny));

        assign_capability('moodle/grade:view', CAP_PROHIBIT, $studentrole = 5, $coursecontext);
        accesslib_clear_all_caches(true);

        $this->assertFalse(has_capability('moodle/grade:view', $coursecontext, $studentdeny));

        $dto = rule_dto::create()
            ->set_user_id($studentdeny->id)
            ->set_role_name('rolename')
            ->set_capabilities_to_allow(['moodle/grade:view'])
            ->set_context($coursecontext)
            ->set_rule_id($ruleid = 1);
        rights_manager::create($blockid = 1)->change_access($dto);
        accesslib_clear_all_caches(true);

        $this->assertFalse(has_capability('moodle/grade:view', $coursecontext, $studentdeny));
    }

    /** @test */
    function it_should_not_allow_permission_for_student_with_prohibit_capability_in_the_child_context() {
        $this->resetAfterTest();

        list($studentdeny, $course) = $this->setup_course_with_student();
        $this->create_user_and_enrol_him_to_course($course->id);
        $coursecontext = context_course::instance($course->id);
        $systemcontext = context_system::instance();

        static::getDataGenerator()->role_assign($userrole = 1, $studentdeny->id, $systemcontext);

        $this->assertTrue(has_capability('moodle/user:manageownfiles', $systemcontext, $studentdeny));

        assign_capability('moodle/user:manageownfiles', CAP_PROHIBIT, $userrole = 1, $systemcontext);
        accesslib_clear_all_caches(true);

        $this->assertFalse(has_capability('moodle/user:manageownfiles', $systemcontext, $studentdeny));

        $dto = rule_dto::create()
            ->set_user_id($studentdeny->id)
            ->set_role_name('rolename')
            ->set_capabilities_to_allow(['moodle/user:manageownfiles'])
            ->set_context($coursecontext)
            ->set_rule_id($ruleid = 1);
        rights_manager::create($blockid = 1)->change_access($dto);
        accesslib_clear_all_caches(true);

        $this->assertFalse(has_capability('moodle/user:manageownfiles', $coursecontext, $studentdeny));
    }

    /** @test */
    function it_should_allow_permission_for_student_in_parent_context_even_if_there_is_prohibit_capability_in_the_child_context() {
        $this->resetAfterTest();

        list($studentallow, $course) = $this->setup_course_with_student();
        $coursecontext = context_course::instance($course->id);
        $categorycontext = $coursecontext->get_parent_context();

        assign_capability('moodle/site:configview', CAP_PROHIBIT, $studentrole = 5, $coursecontext);
        accesslib_clear_all_caches(true);

        $this->assertFalse(has_capability('moodle/site:configview', $coursecontext, $studentallow));
        $this->assertFalse(has_capability('moodle/site:configview', $categorycontext, $studentallow));

        $dto = rule_dto::create()
            ->set_user_id($studentallow->id)
            ->set_role_name('rolename')
            ->set_capabilities_to_allow(['moodle/site:configview'])
            ->set_context($categorycontext)
            ->set_rule_id($ruleid = 1);
        rights_manager::create($blockid = 1)->change_access($dto);
        accesslib_clear_all_caches(true);

        $this->assertTrue(has_capability('moodle/site:configview', $categorycontext, $studentallow));
    }

    /** @test */
    function it_should_change_permission_for_student_when_student_role_is_specified_to_affect() {
        $this->resetAfterTest();

        list($student, $course) = $this->setup_course_with_student();
        $coursecontext = context_course::instance($course->id);

        $this->assertTrue(has_capability('moodle/grade:view', $coursecontext, $student));

        global $DB;
        $studentrole = $DB->get_record('role', ['shortname' => 'student']);

        $dto = rule_dto::create()
            ->set_user_id($student->id)
            ->set_role_name('rolename')
            ->set_capabilities_to_disallow(['moodle/grade:view'])
            ->set_context($coursecontext)
            ->set_role_ids_to_affect([$studentrole->id])
            ->set_role_ids_to_affect_in_context($coursecontext)
            ->set_rule_id($ruleid = 1);
        rights_manager::create($blockid = 1)->change_access($dto);
        accesslib_clear_all_caches(true);

        $this->assertFalse(has_capability('moodle/grade:view', $coursecontext, $student));
    }

    /** @test */
    function it_should_not_change_permission_for_teacher_when_teacher_role_is_not_specified_to_affect() {
        $this->resetAfterTest();

        list($teacher, $course) = $this->setup_course_with_teacher();
        $coursecontext = context_course::instance($course->id);

        $this->assertTrue(has_capability('mod/assign:grade', $coursecontext, $teacher));

        global $DB;
        $studentrole = $DB->get_record('role', ['shortname' => 'student']);

        $dto = rule_dto::create()
            ->set_user_id($teacher->id)
            ->set_role_name('rolename')
            ->set_capabilities_to_disallow(['mod/assign:grade'])
            ->set_context($coursecontext)
            ->set_role_ids_to_affect([$studentrole->id])
            ->set_role_ids_to_affect_in_context($coursecontext)
            ->set_rule_id($ruleid = 1);
        rights_manager::create($blockid = 1)->change_access($dto);
        accesslib_clear_all_caches(true);

        $this->assertTrue(has_capability('mod/assign:grade', $coursecontext, $teacher));
    }

    /** @test */
    function it_should_rollback_deny_permission() {
        $this->resetAfterTest();

        list($student, $course) = $this->setup_course_with_student();
        $coursecontext = context_course::instance($course->id);

        $this->assertTrue(has_capability('moodle/grade:view', $coursecontext, $student));

        $dto = rule_dto::create()
            ->set_user_id($student->id)
            ->set_role_name('rolename')
            ->set_capabilities_to_disallow(['moodle/grade:view'])
            ->set_context($coursecontext)
            ->set_rule_id($ruleid = 1);
        rights_manager::create($blockid = 1)->change_access($dto);
        accesslib_clear_all_caches(true);

        $this->assertFalse(has_capability('moodle/grade:view', $coursecontext, $student));

        rights_manager::create($blockid = 1)->rollback($dto);
        accesslib_clear_all_caches(true);

        $this->assertTrue(has_capability('moodle/grade:view', $coursecontext, $student));
    }

    /** @test */
    function it_should_rollback_allow_permission() {
        $this->resetAfterTest();

        list($student, $course) = $this->setup_course_with_student();
        $coursecontext = context_course::instance($course->id);

        $this->assertFalse(has_capability('moodle/site:configview', $coursecontext, $student));

        $dto = rule_dto::create()
            ->set_user_id($student->id)
            ->set_role_name('rolename')
            ->set_capabilities_to_allow(['moodle/site:configview'])
            ->set_context($coursecontext)
            ->set_rule_id($ruleid = 1);
        rights_manager::create($blockid = 1)->change_access($dto);
        accesslib_clear_all_caches(true);

        $this->assertTrue(has_capability('moodle/site:configview', $coursecontext, $student));

        rights_manager::create($blockid = 1)->rollback($dto);
        accesslib_clear_all_caches(true);

        $this->assertFalse(has_capability('moodle/site:configview', $coursecontext, $student));
    }

    /** @test */
    function it_should_mark_user_as_affected() {
        $this->resetAfterTest();

        list($student, $course) = $this->setup_course_with_student();
        $coursecontext = context_course::instance($course->id);

        $dto = rule_dto::create()
            ->set_user_id($student->id)
            ->set_role_name('rolename')
            ->set_capabilities_to_disallow(['moodle/grade:view'])
            ->set_context($coursecontext)
            ->set_rule_id($ruleid = 1);
        rights_manager::create($blockid = 1)->change_access($dto);

        $isaffected = affected_users_service::create($blockid)->is_affected($student->id);
        $this->assertTrue($isaffected);

        rights_manager::create($blockid = 1)->rollback($dto);
        accesslib_clear_all_caches(true);

        $isaffected = affected_users_service::create($blockid)->is_affected($student->id);
        $this->assertFalse($isaffected);
    }

    /** @test */
    function it_should_not_ignore_user_which_is_already_affected_by_other_rule() {
        $this->resetAfterTest();

        list($student, $course) = $this->setup_course_with_student();
        $coursecontext = context_course::instance($course->id);

        $dto = rule_dto::create()
            ->set_user_id($student->id)
            ->set_role_name('rolename')
            ->set_capabilities_to_disallow(['moodle/grade:view'])
            ->set_context($coursecontext)
            ->set_rule_id($ruleid = 1);
        rights_manager::create($blockid = 1)->change_access($dto);

        assign_capability('moodle/site:configview', CAP_ALLOW, $studentrole = 5, $coursecontext);

        $dto = rule_dto::create()
            ->set_user_id($student->id)
            ->set_role_name('rolename2')
            ->set_capabilities_to_disallow(['moodle/site:configview'])
            ->set_context($coursecontext)
            ->set_rule_id($ruleid = 2);
        rights_manager::create($blockid = 1)->change_access($dto);

        accesslib_clear_all_caches(true);

        $this->assertFalse(has_capability('moodle/grade:view', $coursecontext, $student));
        $this->assertFalse(has_capability('moodle/site:configview', $coursecontext, $student));
    }
}
